package com.pitjarus.time.utils

object Config {
    const val DIRECTORY_ROOT_NAME = "Time"

    const val DATABASE_NAME = "Time-180052-db"
    const val DATABASE_NAME_ENCRYPTED = "Time-180052-db-encrypted"

    // Shared Preference
    const val PREF_NAME = "Time_pref"
    const val KEY_IS_LOGGED_IN = "key_is_logged_in"
    const val KEY_RADIUS_LIMIT = "key_radius_limit"
    const val KEY_CITY_ID = "key_city_id"
    const val KEY_EMPLOYEE_ID = "key_employee_id"
    const val KEY_EMPLOYEE_USERNAME = "key_employee_username"
    const val KEY_EMPLOYEE_NAME = "key_employee_name"
    const val KEY_EMPLOYEE_BRANCH_ID = "key_employee_role_id"
    const val KEY_EMPLOYEE_RADIUS_VERIFICATION_LIMIT = "key_employee_radius_verification_limit"
    const val KEY_VISIT_ID = "key_visit_id"
    const val KEY_IS_ABSENT = "key_is_absent"
    const val KEY_MINGGU_KE = "key_minggu_ke"
    const val KEY_EMPLOYEE_TYPE_ID = "key_employee_type"
    const val KEY_EMPLOYEE_ROLE_ID = "key_employee_role_id"
    const val KEY_EMPLOYEE_ROLE_NAME = "key_employee_role_name"
    const val KEY_VISIT_DATE = "key_visit_date"
    const val KEY_PARENT_ID = "key_parent_id"
    const val KEY_VISIT_DOKTER_ID = "key_visit_dokter_id"
    const val KEY_TERITORY_CODE = "key_teritory_code"


    // Complaint Status
    const val COMPLAINT_STATUS_NEW = "N"
    const val COMPLAINT_STATUS_RESPONDED = "R"
    const val COMPLAINT_STATUS_CLOSED = "C"

    const val KEY_EMPLOYEE_USERNAME_SAVED = "key_employee_username_saved"

    const val KEY_BUKA_CALENDER = "key_buka_calender"
    const val KEY_ALARM_DETIK = "key_detik"
    const val KEY_ALARM_MENIT = "key_menit"
    const val KEY_ALARM_JAM = "key_jam"
    const val KEY_HARI = "key_hari"
    const val KEY_ALARM = "key_alarm"
    const val KEY_TAGGING_LOCATION = "key_tagging_location"


    // Store
    const val KEY_STORE_ID = "key_store_id"
    const val KEY_STORE_CODE = "key_store_code"
    const val KEY_STORE_NAME = "key_store_name"
    const val KEY_STORE_CUSTOMER_GROUP_ID = "key_customer_group"
    const val KEY_STORE_PHOTO = "key_store_photo"
    const val KEY_STORE_CHANNEL = "key_store_channel"

    // Signature
    const val KEY_SIGNATURE_PATH = "key_signature_path"
    const val KEY_NOTE = "key_note"

    //    const val KEY_STORE_BRANCH_ID = "key_store_branch_id"
    const val KEY_CHAINSTORE_ID = "key_chainstore_id"
    const val KEY_STORE_TERITORY_ID = "key_teritory_id"
    const val KEY_STORE_SEGMENT_ID = "key_store_segment_id"
    const val KEY_STORE_SEGMENT_NAME = "key_store_segment_name"
    const val KEY_STORE_CATEGORY_ID = "key_store_category_id"
    const val KEY_STORE_CATEGORY_NAME = "key_store_category_name"

    // Visit
    const val VISIT_CODE = 1
    const val NO_VISIT_CODE = 0
    const val VISIT_TYPE_REGULER = 1
    const val VISIT_TYPE_EVENT = 2

    // Absensi Code
    const val ABSEN_SAKIT_TEXT = "Sakit"
    const val ABSEN_IZIN_TEXT = "Izin"
    const val ABSEN_MASUK_TEXT = "Masuk"
    const val ABSEN_STOCK_OPNAME_TEXT = "ABSEN STOCK OPNAME"
    const val ABSEN_PERBANTUAN_TEXT = "PERBANTUAN"
    const val ABSEN_TUKAR_OFF_TEXT = "TUKAR OFF"
    const val ABSEN_CUTI_TEXT = "CUTI"

    // Absensi
    const val ABSEN_SAKIT = "5"
    const val ABSEN_IZIN = "6"
    const val ABSEN_MASUK = "1"
    const val ABSEN_STOCK_OPNAME = "2"
    const val ABSEN_PERBANTUAN = "3"
    const val ABSEN_TUKAR_OFF = "4"
    const val ABSEN_CUTI = "7"

    // Questioner
    const val QUESTIONER_ID_QUIZ = 2

    // Status api response
    const val STATUS_SUCCESS = "success"
    const val STATUS_FAILURE = "failure"
    const val STATUS_ERROR = "error"

    // Network
    const val INTERVAL = (1000 * 10).toLong()
    const val FASTEST_INTERVAL = (1000 * 5).toLong()

    // Location
    const val ACCURACY_LIMIT = 50f
    const val ACCURACY_SMALL_LIMIT = 30f
    const val REQUEST_LOCATION_LIMIT = 10
    const val RADIUS_VERIFICATION_LOCATION = 300

    // Report
    const val RESEND_REPORT_IN_SECONDS = 45
    const val KEY_CHECKED_REPORT_INVESTMENT = "report_planogram"
    const val KEY_CHECKED_REPORT_CHILLER = "report_chiller"
    const val KEY_CHECKED_REPORT_REGULAR = "report_reguler"
    const val KEY_CHECKED_REPORT_STOCK = "report_stock"
    const val KEY_CHECKED_REPORT_ADDITIONAL_REPORT = "report_additional_report"
    const val KEY_CHECKED_REPORT_KUNJUNGAN_DOKTER = "report_kunjungan_dokter"
    const val KEY_DONE_REPORT_DOKETER = "report_doketer"





    const val KEY_CHECKED_SWITCH = "switch"


    const val KEY_IN_REPORT = "in_report"

    const val ITEM_AVAILABLE = 1
    const val ITEM_NOT_AVAILABLE = 0

    // Crash Report
    const val CRASH_REPORT_URI = "http://103.7.226.22/crash_report/index.php/apis/crash/create"

    // Date Format
    val DATE_FORMAT_DATABASE = "y-M-d H:m:s"
    val DATE_FORMAT_yMdHms = "yMdHms"
    val DATE_FORMAT_yMdHmsSSS = "yMdHmsSSS"
    val DATE_FORMAT_DATABASE_WEATHER = "yyyy-MM-dd HH:mm:ss"
    val DATE_FORMAT_PATH = "yMMdd"
    val DATE_FORMAT_yyMMdd = "yyMMdd"



    // Storage
    const val REQUEST_CODE_WRITE_EXTERNAL_STORAGE = 10

    // Camera
    const val REQUEST_CODE_CAMERA = 20

    // Yes/No
    const val YES_CODE = 1
    const val NO_CODE = 0
    const val YES = "Yes"
    const val NO = "No"

    //FCM
    //FIREBASE CONFIG
    // global topic to receive app wide push notifications
    const val TOPIC_GLOBAL = "global"

    // broadcast receiver intent filters
    const val REGISTRATION_COMPLETE = "registrationComplete"
    const val PUSH_NOTIFICATION = "pushNotification"

    // id to handle the notification in the notification tray
    const val NOTIFICATION_ID = 100
    const val NOTIFICATION_ID_BIG_IMAGE = 101

    const val SHARED_PREF = "ah_firebase"
}
