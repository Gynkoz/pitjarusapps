package com.pitjarus.time.utils

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.provider.Settings
import android.widget.Spinner
import com.pitjarus.time.R
import com.pitjarus.time.models.Login
import com.pitjarus.time.utils.Config
import org.joda.time.DateTime
import java.io.File
import java.net.NetworkInterface
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*


object Helper {
    val isRooted: Boolean
        get() = findBinary("su")

    val deviceData: String
        get() {
            val osVersion = System.getProperty("os.version")
            val sdk = Build.VERSION.SDK_INT
            val device = Build.DEVICE
            val model = Build.MODEL
            val product = Build.PRODUCT
            return "$osVersion,$sdk,$device,$model,$product"
        }

    fun setSpinnerValue(context: Context, arrayId: Int, spinner: Spinner, value: String) {
        val arr = context.resources.getStringArray(arrayId)
        val index = Arrays.asList(*arr).indexOf(value)
        spinner.setSelection(index)
    }

    fun convertZeroToEmpty(intValue: Int, isCompleted: Boolean): String {
        var value = NumberFormat.getNumberInstance(Locale.US).format(intValue.toLong())
        if (!isCompleted) {
            value = if (intValue == 0) "" else value
        }
        return value
    }

    fun convertZeroToEmpty(intValue: Int): String {
        return if (intValue == 0) "" else intValue.toString()
    }

    fun convertYesNoToNumber(value: String): String {
        return when {
            value.toLowerCase() == "yes" -> "1"
            value.toLowerCase() == "no" -> "0"
            else -> value
        }
    }

    fun getSubtitle(context: Context): String {
        return "v${getVersion(context)}/${Login.getEmployeeName(context)}"
    }

    fun getSubtitleInStore(context: Context): String {
        return "v${getVersion(context)}/${Login.getUsername(context)}" +
                "/${SharedPrefsUtils.getStringPreference(context, Config.KEY_STORE_NAME, "")}"
    }

    fun generateVisitId(surveyorId: Int, storeId: Int): String {
        return "V.$surveyorId.$storeId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateUnplanVisitId(surveyorId: Int, storeId: Int): String {
        return "VUP.$surveyorId.$storeId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateVisitDokter(surveyorId: Int, dokterId: Int): String {
        return "VD.$surveyorId.$dokterId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateStoreId(surveyorId: Int, areaId: Int): String {
        return "S.$surveyorId.$areaId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }
    
    fun generateStoreCode(surveyorId: Int, areaId: Int): String {
        return "SC.$surveyorId.$areaId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateReportProductId(surveyorId: Int, poeId: Int): String {
        return "PA.$surveyorId.$poeId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateReportPosmId(surveyorId: Int): String {
        return "PO.$surveyorId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun getDistance(location: Location, longitude: Double, latitude: Double): Double {
        return getDistance(location.longitude, location.latitude, longitude, latitude)
    }

    fun getDistance(longitude1: Double, latitude1: Double, longitude2: Double, latitude2: Double): Double {
        val earthRadius = 6371000.0
        val dLat = Math.toRadians(latitude2 - latitude1)
        val dLng = Math.toRadians(longitude2 - longitude1)
        val a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(latitude1)) * Math.cos(Math.toRadians(latitude2)) *
                Math.sin(dLng / 2) * Math.sin(dLng / 2)
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        return earthRadius * c
    }

    fun <T> getStringArray(list: List<T>): Array<String?> {
        val arr = arrayOfNulls<String>(list.size)

        for (i in list.indices) {
//            arr[i] = (i + 1).toString() + ". " + (list[i] as TTransmissionHistory).getDescription()
        }

        return arr
    }

    fun getVersion(context: Context): String {
        var v = ""
        try {
            v = context.packageManager.getPackageInfo(context.packageName, 0).versionName
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return v
    }

    fun getAndroidId(context: Context): String {
        return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
    }

    private fun findBinary(binaryName: String): Boolean {
        var found = false
        if (!found) {
            val places = arrayOf("/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/")
            for (where in places) {
                if (File(where + binaryName).exists()) {
                    found = true

                    break
                }
            }
        }
        return found
    }

    fun checkPermissionExternalStorage(activity: Context): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
        } else true
    }

    fun removeComma(string: String): String {
        return if (string.contains(",")) {
            string.replace(",", "")
        } else {
            string
        }
    }

    fun formatYesNoToCode(value: String): Int {
        return if (value == Config.YES) {
            1
        } else if (value == Config.NO) {
            0
        } else {
            -1
        }
    }

    fun convertMeterToKm(distance: Float): String {
        val df1 = DecimalFormat("#")
        val df2 = DecimalFormat("#.##")
        return if (distance > 1000) {
            df2.format((distance / 1000).toDouble()) + " km"
        } else {
            df1.format(distance.toDouble()) + " meter"
        }
    }

    fun getIPAddress(useIPv4: Boolean): String {
        try {
            val interfaces = Collections.list(NetworkInterface.getNetworkInterfaces())
            for (intf in interfaces) {
                val addrs = Collections.list(intf.getInetAddresses())
                for (addr in addrs) {
                    if (!addr.isLoopbackAddress()) {
                        val sAddr = addr.getHostAddress()
                        //boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
                        val isIPv4 = sAddr.indexOf(':') < 0

                        if (useIPv4) {
                            if (isIPv4)
                                return sAddr
                        } else {
                            if (!isIPv4) {
                                val delim = sAddr.indexOf('%') // drop ip6 zone suffix
                                return if (delim < 0) sAddr.toUpperCase() else sAddr.substring(0, delim).toUpperCase()
                            }
                        }
                    }
                }
            }
        } catch (ignored: Exception) {
        }
        // for now eat exceptions
        return ""
    }

    fun getComplaintTitleIcon(context: Context, complaintType: String, complaintTitle: String): Int {

        when(complaintType) {

            context.getString(R.string.label_good) -> {
                when(complaintTitle) {
                    context.getString(R.string.label_oos) -> { return R.drawable.img_title_osa_green }
                    context.getString(R.string.label_promo) -> { return R.drawable.img_title_promo_green }
                    context.getString(R.string.label_display) -> { return R.drawable.img_title_display_green }
                    context.getString(R.string.label_spaceshare) -> { return R.drawable.img_title_posm_green }
                    context.getString(R.string.label_price) -> { return R.drawable.img_t_price_good }
                    context.getString(R.string.label_grooming) -> { return R.drawable.img_title_grooming_green }
                }
            }
            context.getString(R.string.label_bad) -> {
                when(complaintTitle) {
                    context.getString(R.string.label_oos) -> { return R.drawable.img_title_osa_red }
                    context.getString(R.string.label_promo) -> { return R.drawable.img_title_promo_red }
                    context.getString(R.string.label_display) -> { return R.drawable.img_title_display_red }
                    context.getString(R.string.label_spaceshare) -> { return R.drawable.img_title_posm_red }
                    context.getString(R.string.label_price) -> { return R.drawable.img_t_price_bad }
                    context.getString(R.string.label_grooming) -> { return R.drawable.img_title_grooming_red }
                }

            }
        }
        return R.drawable.img_t_default_good
    }


    fun generateReportPromoId(surveyorId: Int, promoId: Int): String {
        return "PRO.$surveyorId.$promoId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateReportSewaId(surveyorId: Int, investmentId: Int): String {
        return "SEWA.$surveyorId.$investmentId.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateReportCompetitorId(surveyorId: Int,category: Int): String {
        return "COMP.$surveyorId.$category.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

    fun generateReportFreeId(surveyorId: Int,category: Int): String {
        return "FREE.$surveyorId.$category.${DateTime.now().toString("yyMMddHHmmssSS")}"
    }

}
