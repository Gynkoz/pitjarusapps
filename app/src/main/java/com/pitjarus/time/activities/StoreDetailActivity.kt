package com.pitjarus.time.activities

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.hardware.Camera
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.pitjarus.time.models.Login
import com.pitjarus.time.GlobalApp
import com.pitjarus.time.R
//import com.pitjarus.time.adapters.LastVisitAdapter
import com.pitjarus.time.database.*
import com.pitjarus.time.reports.GenericReport
import com.pitjarus.time.utils.*
import kotlinx.android.synthetic.main.activity_store_detail.*
import org.joda.time.DateTime
import org.joda.time.LocalDate
import java.util.ArrayList

class StoreDetailActivity : AppCompatActivity(),
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener{

    companion object {
        const val REQUEST_CODE_CAMERA = 10
        const val REQUEST_CODE_TAG_LOCATION = 20
        const val ACTION_TYPE_VISIT = "visit"
        const val ACTION_TYPE_NO_VISIT = "novisit"
    }

    private var progressDialog: ProgressDialog? = null

    private var mPhotoStorePath: String? = null
    private var mPhotoType: Int = 0
    private var isAvailablePhotoStore: Boolean = false
    private var isAvailableStoreLocation: Boolean = false
    private var isAvailableLocation = false
    private var mActionType: String? = null
    private var mVisitId: String? = null

    private lateinit var daoSession: DaoSession
    private lateinit var mStore: TStore
    private lateinit var visit: TVisit
    private lateinit var mDokter: MutableList<TDokter>

    private var mLocationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mCurrentLocation: Location? = null
    private val mStoreLocation = Location("StoreLocation")
    private var resetLokasi=false
    private var radiusOk=false
    private var requestLocationCount:Int=0

    private var latLama = 0.0
    private var longLama = 0.0
    private var radiusVerificationLimit: Int = 0
    private var mType = 0



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_detail)

        setSupportActionBar(mtoolbar as Toolbar?)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_store_detail)
        supportActionBar!!.subtitle = Helper.getSubtitle(this)

        initLocationRequest()

        initData()
        initView()
    }

    private fun initData() {
        val storeId = intent.getIntExtra("StoreId", 0)
        val locationStatus=intent.getIntExtra("locationStatus",0)

        if(locationStatus==1){
            radiusOk=true
            store_btnLocation.hide()
//            lblTagging.visibility=View.GONE
            btnRefresh.visibility=View.GONE
        }
        daoSession = (application as GlobalApp).daoSession!!
        mStore = daoSession.tStoreDao.queryBuilder().where(TStoreDao.Properties.StoreId.eq(storeId)).limit(1).unique()
        mDokter = daoSession.tDokterDao.queryBuilder().where(TDokterDao.Properties.IsDokterVisit.eq(1)).list()
        isAvailableLocation = true
        isAvailableStoreLocation = !(mStore.latitude == 0.toDouble() || mStore.longitude == 0.toDouble())
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        progressDialog = ProgressDialog(this)

        radiusVerificationLimit = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_RADIUS_VERIFICATION_LIMIT, 0)
        mType = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_ROLE_ID,0)
    }

    private fun initView() {
        lblStoreName.text = mStore.storeName
        lblStoreInfo.text = "${mStore.storeCode}, ${mStore.territoryName}"
        valueLastVisit.text = mStore.lastVisited
//        store_lblAddress.text = if (mStore.address == "") {
//            "{alamat}"
//        } else {
//            "${mStore.address}, ${mStore.workplaceName}"
//        }

//        valueLastVisit.text = if (mStore.lastVisited == ""||mStore.lastVisited == null) {
//            "{Last Visit}"
//        } else {
//            "${mStore.lastVisited}"
//        }
        if (isAvailableStoreLocation) {
            lblLocation.text = "${mStore.latitude}, ${mStore.longitude}"
            //store_btnLocation.visibility = View.GONE
        } else {
            lblLocation.text = "Lokasi(latitude, longitude) tidak tersedia"
            //store_btnLocation.visibility = View.VISIBLE
            isAvailableLocation = false
        }

        store_btnPhoto.setOnClickListener {
            mPhotoType = REQUEST_CODE_CAMERA
            startCamera(mPhotoType)
        }

        store_btnLocation.setOnClickListener {
            startLocationUpdates(REQUEST_CODE_TAG_LOCATION)
        }

        store_btnNavigation.setOnClickListener {
            var intent = Intent(android.content.Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?daddr=${mStore.latitude},${mStore.longitude}"));
            startActivity(intent)
        }

        store_btnVisit.setOnClickListener {
            Log.i("tagg",SharedPrefsUtils.getIntegerPreference(this@StoreDetailActivity, Config.KEY_TAGGING_LOCATION, 0).toString())

            validateForm(ACTION_TYPE_VISIT)
        }
        store_btnNoVisit.setOnClickListener {
            validateForm(ACTION_TYPE_NO_VISIT)
        }
        btnRefresh.setOnClickListener{
            val popUp = DataController.getPopup(this@StoreDetailActivity, "Perhatian", "Anda Yakin Mau Mengubah Lokasi Toko Ini? Perubahan Lokasi Akan Dicatat Datanya")
            popUp.setPositiveButton("Yakin") { dialogInterface, i ->
                if (mStore != null) {
                    latLama = mStore.latitude
                    longLama = mStore.longitude
                    mStore.latitude = -1000.0
                    mStore.longitude = -1000.0


                    Toast.makeText(this@StoreDetailActivity, "Perubahan Reset Lokasi Berhasil Klik Cek GPS Untuk Melanjutkan Kunjungan", Toast.LENGTH_LONG).show()
                    resetLokasi = true
//                    update_location = true
                    isAvailableStoreLocation=false
                } else
                    Toast.makeText(this@StoreDetailActivity, "Terjadi Error", Toast.LENGTH_LONG).show()
            }
            popUp.setNegativeButton("Tidak") { dialogInterface, i -> dialogInterface.dismiss() }
            popUp.show()
        }

//        reloadList()
    }

//    private fun reloadList() {
//        var items = daoSession.tLastVisitDao.queryBuilder().where(TLastVisitDao.Properties.StoreId.eq(mStore.storeId)).limit(5).list()
//        val adapter = LastVisitAdapter(items, R.layout.last_visit_list_row, object : LastVisitAdapter.OnItemClickListener {
//            override fun onItemClick(item: TLastVisit) {
//            }
//        })
//        recycler.layoutManager = LinearLayoutManager(this)
//        recycler.adapter = adapter
//        adapter.notifyDataSetChanged()
//    }

    private fun validateForm(actionType: String) {
        when (actionType) {
            ACTION_TYPE_VISIT -> {
                //region Validation

                if (!isAvailableLocation) {
                    Toast.makeText(this, String.format(getString(R.string.info_msg_required_update_location)), Toast.LENGTH_SHORT).show()
                    return
                }

                if(!radiusOk && SharedPrefsUtils.getIntegerPreference(this@StoreDetailActivity, Config.KEY_TAGGING_LOCATION, 0) == 1){

                    Toast.makeText(this, String.format(getString(R.string.label_warning_radius)), Toast.LENGTH_SHORT).show()
                    return
                }
                if (!isAvailablePhotoStore) {
                    Toast.makeText(this, String.format(getString(R.string.info_msg_required_photo), "toko"), Toast.LENGTH_SHORT).show()
                    return
                }
                //endregion

                val builder = android.app.AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.label_confirm))
                builder.setMessage(getString(R.string.confirm_msg_visit))
                builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->
                    mActionType = actionType
                    submitForm(actionType, "0")
                }
                builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
                val alertDialog = builder.create()
                alertDialog.setCanceledOnTouchOutside(false)
                alertDialog.show()
            }
            ACTION_TYPE_NO_VISIT -> {

//                if (!isAvailableLocation) {
//                    Toast.makeText(this, String.format(getString(R.string.info_msg_required_update_location)), Toast.LENGTH_SHORT).show()
//                    return
//                }
//
//                if(!radiusOk){
//                    Toast.makeText(this, String.format(getString(R.string.label_warning_radius)), Toast.LENGTH_SHORT).show()
//                    return
//                }
//
//                if (!isAvailablePhotoStore) {
//                    Toast.makeText(this, String.format(getString(R.string.info_msg_required_photo), "toko"), Toast.LENGTH_SHORT).show()
//                    return
//                }
                if (mType == 3 ||mType == 1) {
                    selectReasonDokter()
                }
                else {
                    selectReason()
                }
            }
        }
    }

    private fun submitForm(actionType: String, reasonNoVisit: String) {
        visit(actionType, reasonNoVisit)
    }

    private fun visit(actionType: String, reasonNoVisit: String) {
        val currentUtc = DateTimeUtils.currentUtc
        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
        val currentDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        val reportId = "V" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
        val reportIdPhoto = "VP" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)

        if(mStore.isMandatory==1){
            mVisitId = Helper.generateVisitId(Login.getUserId(this), mStore.storeId)
        }
        else{
            mVisitId = Helper.generateUnplanVisitId(Login.getUserId(this), mStore.storeId)
        }

        val reports = ArrayList<GenericReport>()
        var namaReport = "Visit"
        var isVisit = 1
        if (actionType == ACTION_TYPE_NO_VISIT) {
            isVisit = 0
            namaReport = "No Visit"
        }

        var namaReportPhoto = namaReport+" Photo"


        //Insert visit
        visit = TVisit()
        visit.visitId = mVisitId
        visit.surveyorId = Login.getUserId(this)
        visit.storeId = mStore.storeId
        visit.isVisit = isVisit
        visit.isCheckIn = Config.YES_CODE
        visit.reasonNoVisit = reasonNoVisit
        visit.startDateTimeUtc = currentUtc
        visit.endDateTimeUtc = currentUtc
        visit.startDateTime = currentDateTime
        visit.endDateTime = currentDateTime
        visit.photoPath = mPhotoStorePath

        when (mActionType) {
            ACTION_TYPE_VISIT -> {
                if (!isAvailableStoreLocation) {
                    mStore.latitude = mCurrentLocation!!.latitude
                    mStore.longitude = mCurrentLocation!!.longitude
                    daoSession.tStoreDao.update(mStore)
                }
                daoSession.tVisitDao.insert(visit)
                Login.clearSharedPrefVisit(this)
                SharedPrefsUtils.setStringPreference(this, Config.KEY_VISIT_ID, mVisitId!!)
                SharedPrefsUtils.setIntegerPreference(this, Config.KEY_STORE_ID, mStore.storeId)
                SharedPrefsUtils.setIntegerPreference(this, Config.KEY_STORE_CHANNEL, mStore.channelId)
                SharedPrefsUtils.setStringPreference(this, Config.KEY_STORE_NAME, mStore.storeName)
                SharedPrefsUtils.setStringPreference(this, Config.KEY_STORE_TERITORY_ID, mStore.territoryCode)
                SharedPrefsUtils.setStringPreference(this, Config.KEY_IN_REPORT, getString(R.string.label_category_list))
                SharedPrefsUtils.setStringPreference(this, Config.KEY_STORE_PHOTO,mPhotoStorePath!!)
                SharedPrefsUtils.setIntegerPreference(this, Config.KEY_STORE_CUSTOMER_GROUP_ID,mStore.customerGroupId)

                val datePath = DateTime.now().toString(Config.DATE_FORMAT_PATH)
                SharedPrefsUtils.setStringPreference(this, Config.KEY_VISIT_DATE,datePath)

                if( (mType==3 && mStore.isVisited==1) || (mType==3 && mStore.isStoreVisit==1)) {
                    SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_ADDITIONAL_REPORT, true)
                    SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_STOCK, true)
//                        var dokters=daoSession.tDokterDao.queryBuilder().where(TDokterDao.Properties.IsDone.eq(1))
//                        var DokterVisited=daoSession.tDokterDao.queryBuilder().where(TDokterDao.Properties.IsDokterVisit.eq(1))
                }else {
                    SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_ADDITIONAL_REPORT, false)
                    SharedPrefsUtils.setBooleanPreference(this, Config.KEY_CHECKED_REPORT_STOCK, false)
                    var dokters=daoSession.tDokterDao.queryBuilder().list()
                    for (o in dokters){
                        o.isDone=false
                        daoSession.tDokterDao.update(o)
                    }
                }

                var sku=daoSession.tSkuDao.queryBuilder().list()
                for (o in sku){
                    o.isDone=false
                    daoSession.tSkuDao.update(o)
                }

                var brands=daoSession.tBrandsDao.queryBuilder().list()
                for (o in brands){
                    o.isDone=false
                    daoSession.tBrandsDao.update(o)
                }

                val intent = Intent(this, MainMenuActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                finish()
            }
            ACTION_TYPE_NO_VISIT -> {
                mStore.isVisited = Config.YES_CODE
                daoSession.tStoreDao.update(mStore)
                daoSession.tVisitDao.insert(visit)
                finish()
            }
        }

    }

//    private fun updateStoreLocation(latitude: Double, longitude: Double) {
//        mActionType = "update location"
//        val currentUtc = DateTimeUtils.currentUtc
//        val currentDate = LocalDate.now().toString("yyyy-MM-dd")
//        val reportId = "USL" + DateTime.now().toString(Config.DATE_FORMAT_yMdHmsSSS)
//
//        val params = ArrayList<ReportParameter>()
//        params.add(ReportParameter("1", reportId, "store_id", mStore.storeId.toString(), ReportParameter.TEXT))
//        params.add(ReportParameter("2", reportId, "latitude", latitude.toString(), ReportParameter.TEXT))
//        params.add(ReportParameter("3", reportId, "longitude", longitude.toString(), ReportParameter.TEXT))
//
//        val report = GenericReport(reportId,
//                Login.getUserId(this).toString(),
//                "Update Location",
//                "Update Location: " + mStore.storeId + " (" + DateTimeUtils.transformFullTime(DateTimeUtils.waktuInLong) + ")",
//                ApiClient.getUpdateStoreLocationUrl(),
//                currentDate, Config.NO_CODE, currentUtc, params)
//
//        val task = TambahReportTask(this)
//        task.execute(report)
//    }

    private fun selectReason() {
        val reasons = daoSession.tReasonNoVisitDao.queryBuilder().where(TReasonNoVisitDao.Properties.ReasonGroup.`in`(1,3)).list()
        val items = arrayOfNulls<String>(reasons.size + 1)
        var i = 0
        reasons.forEach {
            items[i] = "${it.reasonName}"//"${it.reasonId}. ${it.reasonName}"
            i++
        }
        items[reasons.size] = "Cancel"

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Pilih alasan")
        builder.setItems(items) { dialog, item ->
            dialog.dismiss()
            if (items[item] != "Cancel") {
                mActionType = ACTION_TYPE_NO_VISIT
                val separated = items[item]!!.split(".")
                submitForm(ACTION_TYPE_NO_VISIT, separated[0])
            }
        }
        builder.show()
    }

    private fun selectReasonDokter() {
        val reasons = daoSession.tReasonNoVisitDao.queryBuilder().where(TReasonNoVisitDao.Properties.ReasonGroup.`in`(2,3)).list()
        val items = arrayOfNulls<String>(reasons.size + 1)
        var i = 0
        reasons.forEach {
            items[i] = "${it.reasonName}"//"${it.reasonId}. ${it.reasonName}"
            i++
        }
        items[reasons.size] = "Cancel"

        val builder = AlertDialog.Builder(this)
        builder.setTitle("Pilih alasan")
        builder.setItems(items) { dialog, item ->
            dialog.dismiss()
            if (items[item] != "Cancel") {
                mActionType = ACTION_TYPE_NO_VISIT
                val separated = items[item]!!.split(".")
                submitForm(ACTION_TYPE_NO_VISIT, separated[0])
            }
        }
        builder.show()
    }

    private fun callIntentCamera() {
        val intent = Intent(this, PhotoActivity::class.java)
        intent.putExtra("nama_file", "store_" + mPhotoType + "_" + DateTime.now().toString("yyyyMMddHHmmssSSS"))
        intent.putExtra("CameraFacing", Camera.CameraInfo.CAMERA_FACING_FRONT)
        startActivityForResult(intent, mPhotoType)
    }

    private fun startCamera(requestCamera: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                    Toast.makeText(this, "Camera permission is needed to show the camera preview.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.CAMERA), requestCamera)
            }
        } else {
            callIntentCamera()
        }
    }

    private fun initLocationRequest() {
        if (!GooglePlayUtils.isGooglePlayServicesAvailable(this)) {
            finish()
            try {
                Toast.makeText(this, getString(R.string.info_msg_google_play_service_not_available), Toast.LENGTH_LONG).show()
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.uri_market_google_play_service))))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.uri_store_google_play_service))))
            }

        }
        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
    }

    private fun startLocationUpdates(requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    try {
                        mLocationManager!!.removeTestProvider(LocationManager.GPS_PROVIDER)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
//                    store_btnLocation.isEnabled = false
                    isAvailableLocation = false
                    store_lblLoading.visibility = View.VISIBLE
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_grey_24dp, getApplicationContext().getTheme()));
                    } else {
                        store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_grey_24dp));
                    }
//                    store_btnLocation.setImageResource(R.drawable.ic_my_location_grey_24dp)
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
                } else {
                    showGPSSetting()
                }
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(this, "Location permission is needed to get current location.", Toast.LENGTH_SHORT).show()
                }

                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCode)
            }
        } else {
            if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
//                store_btnLocation.isEnabled = false
                isAvailableLocation = false
                store_lblLoading.text = getString(R.string.info_msg_search_location)
                store_lblLoading.visibility = View.VISIBLE
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_grey_24dp, getApplicationContext().getTheme()));
                } else {
                    store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_grey_24dp));
                }
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            } else {
                showGPSSetting()
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
        }



        return super.onOptionsItemSelected(item)
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = Config.INTERVAL
        mLocationRequest!!.fastestInterval = Config.FASTEST_INTERVAL
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun stopLocationUpdates() {
        if (mGoogleApiClient!!.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
//            store_btnLocation.isEnabled = true
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            if (!isAvailableLocation) {
                store_lblLoading.visibility = View.INVISIBLE
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp, getApplicationContext().getTheme()));
                } else {
                    store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp));
                }               // store_btnLocation.isEnabled = false
            }
        }
    }

    private fun showGPSSetting() {
        if (!mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            val builder = android.app.AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_gps_not_available))
            builder.setMessage(getString(R.string.info_msg_required_gps))
            builder.setPositiveButton(getString(R.string.label_gps_setting)) { _, _ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            builder.setNegativeButton(getString(R.string.label_cancel)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }
    }


    override fun onLocationChanged(location: Location) {
        if (LocationUtils.isBetterLocation(location, mCurrentLocation)) {
            mCurrentLocation = location
            if (LocationUtils.isFromMockProvider(this, location)) {
                stopLocationUpdates()
                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
//                store_btnLocation.isEnabled = true
                Toast.makeText(this, "Anda terdeteksi menggunakan aplikasi yang bekerja memanipulasi lokasi, harap dinonaktifkan!", Toast.LENGTH_LONG).show()
            } else {
                if (!isAvailableStoreLocation) { // Jika tidak ada lokasi (long lat)
                    if (location.accuracy <= Config.ACCURACY_SMALL_LIMIT) {
                        stopLocationUpdates()
                        mCurrentLocation = location
                        isAvailableLocation = true
                        isAvailableStoreLocation = true
                        radiusOk=true
                        store_btnLocation.setEnabled(true)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp, getApplicationContext().getTheme()));
                        } else {
                            store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp));
                        }
                        lblLocation.text = "${location.latitude}, ${location.longitude}"
//
                        mStoreLocation.latitude = mStore.latitude
                        mStoreLocation.longitude = mStore.longitude
                        mStore.latitude = location.latitude
                        mStore.longitude = location.longitude
                        daoSession.tStoreDao.update(mStore)

//                        updateStoreLocation(mStore.latitude, mStore.longitude)

                        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    }
                } else {
                    if (location.accuracy <= Config.ACCURACY_SMALL_LIMIT) {
                        if (requestLocationCount == Config.REQUEST_LOCATION_LIMIT) {
                            stopLocationUpdates()
                            isAvailableLocation = true
                            isAvailableStoreLocation = true
//                            store_btnLocation.isEnabled = true
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp, getApplicationContext().getTheme()));
                            } else {
                                store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp));
                            }
//                            store_btnLocation.setImageResource(R.drawable.ic_my_location_white_24dp)
                            store_lblLoading.setText(getString(R.string.label_klik_untuk_verfikasi_lokasi))
                        } else {
                            mStoreLocation.latitude = mStore.latitude
                            mStoreLocation.longitude = mStore.longitude
                            val distance = location.distanceTo(mStoreLocation)
                            store_lblLoading.setText("Jarak anda dengan store " + Helper.convertMeterToKm(distance))
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp, getApplicationContext().getTheme()));
                            } else {
                                store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp));
                            }
//                            store_btnLocation.setImageResource(R.drawable.ic_my_location_white_24dp)
                            isAvailableLocation = true
                            requestLocationCount++
//                            store_btnLocation.isEnabled = true
                            if (distance <= radiusVerificationLimit) {
                                Log.i("distance",distance.toString())
                                stopLocationUpdates()
                                mCurrentLocation = location
                                radiusOk=true
//                                store_btnLocation.isEnabled = true
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp, getApplicationContext().getTheme()));
                                } else {
                                    store_btnLocation.setImageDrawable(getResources().getDrawable(R.drawable.ic_my_location_white_24dp));
                                }
                                store_lblLoading.setText("Jarak anda dengan store " + Helper.convertMeterToKm(distance))
                                store_lblLoading.visibility=View.VISIBLE
                                window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                            }
                        }
                    }
                }

            }
        }
    }

    public override fun onResume() {
        super.onResume()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    public override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
        mGoogleApiClient!!.disconnect()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onConnected(bundle: Bundle?) {}

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == REQUEST_CODE_CAMERA && resultCode == Activity.RESULT_OK) {
                mPhotoStorePath = data?.extras!!.getString("path")
                store_view.setImageBitmap(BitmapFactory.decodeFile(mPhotoStorePath))
                store_view.scaleType = ImageView.ScaleType.CENTER_CROP
                isAvailablePhotoStore = true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            isAvailablePhotoStore = false
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == REQUEST_CODE_CAMERA) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                callIntentCamera()
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }

        if (requestCode == REQUEST_CODE_TAG_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startLocationUpdates(REQUEST_CODE_TAG_LOCATION)
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

}