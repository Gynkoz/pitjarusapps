package com.pitjarus.time.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.ListView
import com.pitjarus.time.models.Login
import com.pitjarus.time.R
import com.pitjarus.time.reports.DatabaseReport
import com.pitjarus.time.reports.ReportUploader
import com.pitjarus.time.utils.*
import kotlinx.android.synthetic.main.activity_transmission_history.*
import org.joda.time.DateTime
import java.io.File
import java.util.*
import android.widget.Toast


class TransmissionActivity : AppCompatActivity() {

    private lateinit var reciever: MyBroadcastReciever
    private lateinit var toSend: Array<String>
    private lateinit var sent: Array<String>

    private var adapterTerkirim: ArrayAdapter<String>? = null
    private var adapterBelumTerkirim: ArrayAdapter<String>? = null
    private var databaseReport: DatabaseReport? = null
    private var listViewtoBeSent: ListView? = null
    private var listViewSent: ListView? = null
    private var switchOn=false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transmission_history)
        setSupportActionBar(mtoolbar as Toolbar?)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_transmission_history)
        supportActionBar!!.subtitle = Helper.getSubtitle(this)

        init()
    }

    override fun onResume() {
        super.onResume()
        val filter = IntentFilter(Intent.ACTION_SEND)
        reciever = MyBroadcastReciever(this)
        registerReceiver(reciever, filter)
        update()
    }

    private fun init() {
        listViewSent = findViewById(R.id.transmission_listView_sent)
        listViewtoBeSent = findViewById(R.id.transmission_listView_tobesent)
        listViewSent?.emptyView = DataController.getEmptyView(this)
        listViewtoBeSent?.emptyView = DataController.getEmptyView(this)
        transmission_lblVersion.text = "Ver${Helper.getVersion(this)}"

        switchOn=SharedPrefsUtils.getBooleanPreference(this, Config.KEY_CHECKED_SWITCH, false)
        Log.i("switch",switchOn.toString())

        update()
    }

    fun update() {
        if (!isLoading) {
            val thread = Thread(Runnable {
                isLoading = true
                try {
                    databaseReport = DatabaseReport.getDatabase(applicationContext)

                    toSend = DatabaseReport.getDeskripsi(databaseReport?.reportToBeSent!!)
                    sent = DatabaseReport.getDeskripsi(databaseReport?.reportSent!!)

                    runOnUiThread {
                        if (adapterBelumTerkirim != null) {
                            adapterBelumTerkirim!!.clear()
                            adapterBelumTerkirim!!.addAll(*toSend)
                            adapterBelumTerkirim!!.notifyDataSetChanged()
                        } else {
                            val itemToSend = ArrayList(Arrays.asList(*toSend))
                            adapterBelumTerkirim = ArrayAdapter(this@TransmissionActivity, R.layout.activity_transmission_list_row, itemToSend)
                            listViewtoBeSent?.adapter = adapterBelumTerkirim
                        }

                        if (adapterTerkirim != null) {
                            adapterTerkirim!!.clear()
                            adapterTerkirim!!.addAll(*sent)
                            adapterTerkirim!!.notifyDataSetChanged()
                        } else {
                            val itemSent = ArrayList(Arrays.asList(*sent))
                            adapterTerkirim = ArrayAdapter(this@TransmissionActivity, R.layout.activity_transmission_list_row, itemSent)
                            listViewSent?.adapter = adapterTerkirim

                        }
                    }

                    Log.i("accepet reciever", "eksekusi reciever")

                } catch (e: Exception) {
                    e.printStackTrace()

                } finally {
                    isLoading = false
                }
            })

            thread.start()
        }
    }


    override fun onPause() {
        super.onPause()
        unregisterReceiver(reciever)

    }

    class MyBroadcastReciever : BroadcastReceiver {
        private var act: TransmissionActivity? = null

        constructor()

        constructor(activity: TransmissionActivity) {
            act = activity
        }

        override fun onReceive(context: Context, intent: Intent) {
            context.startService(Intent(context, ReportUploader::class.java))
            if (act != null)
                act!!.update()
        }

    }

    companion object {
        var isLoading: Boolean = false
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_transmission, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            finish()
        }

        if (id == R.id.action_send_email) {
        }

        if(id == R.id.action_refresh_transmission){
            val iService = Intent(applicationContext, ReportUploader::class.java)
            startService(iService)
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
