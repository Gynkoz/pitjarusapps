package com.pitjarus.time.activities

import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.*
import com.pitjarus.time.GlobalApp
import com.pitjarus.time.R
import com.pitjarus.time.database.*
import com.pitjarus.time.utils.Config
import com.pitjarus.time.utils.Helper
import com.pitjarus.time.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.activity_main_menu.*
import java.io.File

class MainMenuActivity : AppCompatActivity() {

    private var mType = 0
    private var mPhotoStorePath: String?=null
    private var mStoreId = 0
    private var mVisitId: String? = ""
    private var isFromNotification = false

    private lateinit var daoSession: DaoSession
    private lateinit var mStore: TStore
    private lateinit var items: List<TDokter>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)

        setSupportActionBar(mtoolbar as Toolbar?)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_main_menu)
        supportActionBar!!.subtitle = Helper.getSubtitleInStore(this)

        initData()
        initView()
    }

    private fun initData(){
        daoSession = (application as GlobalApp).daoSession!!
        mPhotoStorePath=SharedPrefsUtils.getStringPreference(this, Config.KEY_STORE_PHOTO,"")
        mType = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_ROLE_ID,0)
        mStoreId = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_STORE_ID, 0)
        mVisitId = SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID, "")
        mStore = daoSession.tStoreDao.queryBuilder().where(TStoreDao.Properties.StoreId.eq(mStoreId)).limit(1).unique()
        items=daoSession.tDokterDao.queryBuilder().where(TDokterDao.Properties.StoreId.eq(mStore.storeId)).list()
        isFromNotification = intent.getBooleanExtra("FromNotification2",false)

        if (mPhotoStorePath!=null && File(mPhotoStorePath).exists()) {
            imgStore.setImageBitmap(BitmapFactory.decodeFile(mPhotoStorePath))
        }

    }

    private fun initView() {
        lblStoreName.text = mStore.storeName
        lblStoreInfo.text = "${mStore.storeCode}, ${mStore.territoryName}"

        btnStock.visibility = View.GONE
        btnAdditionalReport.visibility = View.GONE
        txtSuccess.visibility = View.GONE
        recycler.visibility=View.GONE
        lblDokter.visibility=View.GONE
        txtSuccess.visibility = View.VISIBLE

        btnSelesai.setOnClickListener {
            submitForm()
        }
}

    private fun submitForm() {
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
        finish()
    }

}
