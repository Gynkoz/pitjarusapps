package com.pitjarus.time.activities

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.*
import com.pitjarus.time.GlobalApp
import com.pitjarus.time.R
import com.pitjarus.time.database.DaoSession
import com.pitjarus.time.database.TAbsensi
import com.pitjarus.time.models.Login
import com.pitjarus.time.utils.Config
import com.pitjarus.time.utils.Helper
import com.pitjarus.time.utils.SharedPrefsUtils
import kotlinx.android.synthetic.main.activity_absensi.*
import org.joda.time.DateTime
import java.util.*

class AbsensiActivity : AppCompatActivity() {

    private var progressDialog: ProgressDialog? = null
    private var isFromStoreList = false
    private var mLastClickTime: Long = 0
    private val cal = Calendar.getInstance()
    private var isFromNotification = false

    private lateinit var daoSession: DaoSession
    private lateinit var absensi: TAbsensi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absensi)

        setSupportActionBar(mtoolbar as Toolbar?)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_absensi)
        supportActionBar!!.subtitle = Helper.getSubtitle(this)


//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            var window = getWindow();
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            window.setStatusBarColor(Color.TRANSPARENT);
//        }

        initData()
        initView()

    }

    private fun initData(){
        daoSession = (application as GlobalApp).daoSession!!
        absensi = TAbsensi()
        isFromStoreList = intent.getBooleanExtra("StoreList",false)
        isFromNotification = intent.getBooleanExtra("FromNotification2",false)

    }
    private fun initView(){

        userNameLbl.text = Login.getEmployeeName(this)
        codeUserLbl.text = Login.getEmployeeRoleName(this)

        if(isFromStoreList){
            masukBtn.visibility=View.GONE
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        }
        else{
            masukBtn.visibility=View.VISIBLE
        }

            masukBtn.setOnClickListener{
            SharedPrefsUtils.setIntegerPreference(this, Config.KEY_IS_ABSENT, 1)

            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_confirm))
            builder.setMessage(getString(R.string.confirm_msg_absen_masuk))
            builder.setPositiveButton(getString(R.string.label_yes)) { _, i ->
                submitForm()
            }
            builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }

    }

    private fun submitForm(){

        absensi.absensiDateTime = DateTime.now().toString(Config.DATE_FORMAT_DATABASE)
        daoSession.tAbsensiDao.insert(absensi)
        val iLogin = Intent(this@AbsensiActivity, StoreListActivity::class.java)
        startActivity(iLogin)
        finish()
    }

}
