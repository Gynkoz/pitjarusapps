package com.pitjarus.time.activities

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.google.firebase.iid.FirebaseInstanceId
import com.pitjarus.time.GlobalApp
import com.pitjarus.time.R
import com.pitjarus.time.database.*
import com.pitjarus.time.models.Login
import com.pitjarus.time.models.LoginResponse
import com.pitjarus.time.reports.DatabaseReport
import com.pitjarus.time.reports.ReportUploader
import com.pitjarus.time.rests.ApiClient
import com.pitjarus.time.rests.LoginApiInterface
import com.pitjarus.time.services.NotifyWorker
import com.pitjarus.time.utils.*
import kotlinx.android.synthetic.main.activity_login.*
import org.joda.time.DateTime
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import java.util.concurrent.TimeUnit

class LoginActivity : AppCompatActivity() {
    private lateinit var mAndroidId: String
    private lateinit var mAppVersion: String
    private lateinit var mDeviceData: String
    private lateinit var daoSession: DaoSession

    private var progressDialog: ProgressDialog? = null
    private var usernameSaved=false
    private var ipAddress=""
    private var regId=""
    private var androidVersion=0
    private var hari=0
    private var currentDate = DateTime.now().toString("dd").toInt()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            var window = getWindow()
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.setStatusBarColor(Color.TRANSPARENT)
        }

        initData()
        initView()
    }

    private fun initData(){
        checkStoragePermission()
        mAndroidId = Helper.getAndroidId(this)
        mAppVersion = Helper.getVersion(this)
        mDeviceData = Helper.deviceData
        ipAddress= Helper.getIPAddress(true)
        androidVersion=Build.VERSION.SDK_INT
        hari = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_HARI,0)

        var fromNotification = intent.getBooleanExtra("fromNotification",false)

        if (android.os.Build.VERSION.SDK_INT > 9) {
            val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }

        if (Login.isLoggedIn(this)) {
            if(hari==currentDate){
            val intent =if (!SharedPrefsUtils.getStringPreference(this, Config.KEY_VISIT_ID, "").equals("")) {
                when(SharedPrefsUtils.getStringPreference(this, Config.KEY_IN_REPORT, "")) {
                    getString(R.string.label_main_menu) -> {
                        Intent(this, MainMenuActivity::class.java)
                    }
                    else -> {
                        Intent(this, MainMenuActivity::class.java)
                    }
                }
            } else {
                if(Login.isAbsenIn(this)){
                Intent(this, StoreListActivity::class.java)//StoreListActivity
                }
                else{
                    Intent(this, AbsensiActivity::class.java)
                }
            }
            startActivity(intent)
            finish()
        }
    }

    }

    private fun initView(){
        val appVersion = String.format(getString(R.string.label_app_version), mAppVersion, mAndroidId)
        login_txtAndroidId.text = appVersion

        login_btnLogin.setOnClickListener {
            val username = login_txtUsername.text.toString().trim { it <= ' ' }
            val password = login_txtPassword.text.toString().trim { it <= ' ' }
            if (username != "" && password != "") {
                    val connectionDetectorUtil = ConnectionDetectorUtil(this@LoginActivity)
                    if (connectionDetectorUtil.isConnectingToInternet) {
                        submitForm(username, password)
                    } else {
                        Toast.makeText(
                            this,
                            getString(R.string.info_msg_no_network_connection),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
            } else {
                Toast.makeText(this, getString(R.string.info_msg_required_field_username_password), Toast.LENGTH_SHORT).show()
            }
        }
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(this) { instanceIdResult ->
            val newToken = instanceIdResult.token
            regId=newToken
            Log.e("newToken", newToken)
        }
    }

    private fun submitForm(username: String, password: String) {
        try {
            login_btnLogin.isEnabled = false
            login_progressBar.visibility = View.VISIBLE
            daoSession = (application as GlobalApp).daoSession!!
            daoSession.tTransmissionHistoryDao.deleteAll()

            daoSession.tStoreDao.deleteAll()
            daoSession.tAbsensiDao.deleteAll()
            daoSession.tVisitDao.deleteAll()


            val currentUtc = DateTimeUtils.currentUtc


            val apiService = ApiClient.client!!.create(LoginApiInterface::class.java)
            val call = apiService.getLoginData(username, password, mAndroidId, mAppVersion, mDeviceData, currentUtc, ipAddress, androidVersion, regId)
            call.enqueue(object : Callback<LoginResponse> {
                override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                    if (response.isSuccessful) {
//                        if (response.body().status == Config.STATUS_SUCCESS) {
                            if (response.code().toString() == "500") {
                                try {
                                val surveyor = response.body().surveyor
                                SharedPrefsUtils.clearPreferences(this@LoginActivity)
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ID, surveyor!!.surveyorId)
                                SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_EMPLOYEE_NAME, surveyor.surveyorName)
                                SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_EMPLOYEE_USERNAME, surveyor.username)
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ROLE_ID, surveyor!!.roleId)
                                SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ROLE_NAME, surveyor!!.roleName)
                                SharedPrefsUtils.setStringPreference(this@LoginActivity, Config.KEY_TERITORY_CODE, surveyor!!.surveyorCode)
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_CITY_ID, surveyor.cityId)
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_RADIUS_VERIFICATION_LIMIT, surveyor.radiusVerificationLimit)
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_PARENT_ID, surveyor.parentId)
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_HARI, currentDate)
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_BUKA_CALENDER, surveyor!!.bukaCalender)
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_TAGGING_LOCATION, surveyor!!.taggingGps)
                                var separated = surveyor!!.alarm.split(":")
                                separated[0]
                                separated[1]
                                separated[2]
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_ALARM_JAM, separated[0].toInt())
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_ALARM_MENIT, separated[1].toInt())
                                SharedPrefsUtils.setIntegerPreference(this@LoginActivity, Config.KEY_ALARM_DETIK, separated[2].toInt())


                                SharedPrefsUtils.setBooleanPreference(this@LoginActivity, Config.KEY_ALARM, false)
                                setLogoutReminder()
                                if (response.body().stores != null) {
                                    val storeSize = response.body().stores!!.size
                                        if (storeSize > 0) {
                                            val stores = arrayOfNulls<TStore>(storeSize)
                                            var store: TStore
                                            for ((i, model) in response.body().stores!!.withIndex()) {

                                                store = TStore()
                                                store.storeId = model.storeId
                                                store.storeCode = model.storeCode
                                                store.storeName = model.storeName
                                                store.address = model.address
                                                store.cityId = model.cityId
                                                store.cityName = model.cityName
                                                store.branchId = model.branchId
                                                store.branchName = model.branchName
                                                store.regionId = model.regionId
                                                store.regionName = model.regionName
                                                store.customerGroupId = model.customerGroupId
                                                store.customerGroupName = model.customerGroupName
                                                store.channelId = model.channelId
                                                store.channelName = model.channelName
                                                store.storeClassId = model.storeClassId
                                                store.storeClassName = model.storeClassName
                                                store.territoryCode = model.territoryCode
                                                store.territoryName = model.territoryName
                                                store.latitude = model.latitude
                                                store.longitude = model.longitude
                                                store.lastVisited = model.lastVisited
                                                store.displayColumn = model.displayColumn

                                                store.displayRow = model.displayRow
                                                store.photoPath = model.photoPath
                                                store.isReporting = model.isReporting
                                                store.isMandatory = 1
                                                store.isVisited = Config.NO_CODE
                                                if(SharedPrefsUtils.getIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ROLE_ID,0) == 3 || SharedPrefsUtils.getIntegerPreference(this@LoginActivity, Config.KEY_EMPLOYEE_ROLE_ID,0) == 1){
                                                    store.isStoreVisit = model.isStoreVisit
                                                }
                                                else{
                                                    store.isStoreVisit = 2
                                                }
                                                stores[i] = store
                                            }
                                            daoSession.tStoreDao.insertInTx(stores.toList())
                                        }
                                    }
                                    //End Region

                                    //Store
                                    if (response.body().distributor != null) {
                                        val storeSize = response.body().distributor!!.size
                                        if (storeSize > 0) {
                                            val stores = arrayOfNulls<TStore>(storeSize)
                                            var store: TStore
                                            for ((i, model) in response.body().distributor!!.withIndex()) {

                                                store = TStore()
                                                store.storeId = model.storeId
                                                store.storeCode = model.storeCode
                                                store.storeName = model.storeName
                                                store.address = model.address
                                                store.cityId = model.cityId
                                                store.cityName = model.cityName
                                                store.branchId = model.branchId
                                                store.branchName = model.branchName
                                                store.regionId = model.regionId
                                                store.regionName = model.regionName
                                                store.customerGroupId = model.customerGroupId
                                                store.customerGroupName = model.customerGroupName
                                                store.channelId = model.channelId
                                                store.channelName = model.channelName
                                                store.storeClassId = model.storeClassId
                                                store.storeClassName = model.storeClassName
                                                store.territoryCode = model.territoryCode
                                                store.territoryName = model.territoryName
                                                store.latitude = model.latitude
                                                store.longitude = model.longitude
                                                store.lastVisited = model.lastVisited
                                                store.displayColumn = model.displayColumn
                                                store.isStoreVisit = model.isDitributorVisit
                                                store.displayRow = model.displayRow
                                                store.photoPath = model.photoPath
                                                store.isReporting = model.isReporting
                                                store.isMandatory = 0
                                                store.isVisited = Config.NO_CODE
                                                stores[i] = store
                                            }
                                            daoSession.tStoreDao.insertInTx(stores.toList())
                                        }
                                    }
                                    //End Region

                                    val i = Intent(this@LoginActivity, AbsensiActivity::class.java)//StoreListActivity
                                    startActivity(i)
                                    SharedPrefsUtils.setBooleanPreference(this@LoginActivity, Config.KEY_IS_LOGGED_IN, true)
                                    finish()

                            } catch (e: Exception) {
                                e.printStackTrace()
                                Login.logout(this@LoginActivity)
                                val iLogin = Intent(this@LoginActivity, LoginActivity::class.java)
                                iLogin.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                                startActivity(iLogin)
                                finish()
                                Toast.makeText(this@LoginActivity, e.message, Toast.LENGTH_SHORT).show()
                            }
                        } else if (response.body().status == Config.STATUS_FAILURE) {
                            Toast.makeText(this@LoginActivity, response.body().message, Toast.LENGTH_SHORT).show()
                        }
                        login_progressBar.visibility = View.GONE
                        login_btnLogin.isEnabled = true
                    }
                }

                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    login_progressBar.visibility = View.GONE
                    login_btnLogin.isEnabled = true
                    Toast.makeText(this@LoginActivity, t.message, Toast.LENGTH_SHORT).show()
                }
            })
        }catch (Ex:Exception){
            Ex.printStackTrace()
            login_btnLogin.isEnabled = true
            login_progressBar.visibility = View.GONE
            Toast.makeText(this@LoginActivity,"Permission not granted",Toast.LENGTH_SHORT).show()
        }
    }

    public override fun onResume() {
        super.onResume()
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN)
    }

    public override fun onStart() {
        super.onStart()
    }

    private fun checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                StorageUtils.createDirectories()
                initDaoSession()
                DatabaseReport.getDatabase(this)
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    Toast.makeText(this, "Storage permission is needed to write external storage.", Toast.LENGTH_SHORT).show()
                }
                requestPermissions(arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), Config.REQUEST_CODE_WRITE_EXTERNAL_STORAGE)
            }
        } else {
            StorageUtils.createDirectories()
            DatabaseReport.getDatabase(this)
            val iService = Intent(applicationContext, ReportUploader::class.java)
            startService(iService)
        }
    }

    private fun initDaoSession() {
        if ((application as GlobalApp).daoSession == null) {
            val database = StorageUtils.getDirectory(StorageUtils.DIRECTORY_DATABASE) + "/" + Config.DATABASE_NAME
            val helper = DaoMaster.DevOpenHelper(this, database)
            val db = helper.writableDb
            (application as GlobalApp).daoSession = DaoMaster(db).newSession()
        }
    }

    private fun setLogoutReminder() {
        try {
            // Jam 5.30 Sore
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.SECOND, SharedPrefsUtils.getIntegerPreference(this@LoginActivity, Config.KEY_ALARM_DETIK,0))
            calendar.set(Calendar.MINUTE, SharedPrefsUtils.getIntegerPreference(this@LoginActivity, Config.KEY_ALARM_MENIT, 0))
            calendar.set(Calendar.HOUR_OF_DAY, SharedPrefsUtils.getIntegerPreference(this@LoginActivity, Config.KEY_ALARM_JAM, 0))
            val duration = TimeUnit.MILLISECONDS.toMinutes((calendar.timeInMillis - DateTimeUtils.waktuInLong))
            if (duration > 0) {
                val workTag = "LogoutReminderWork1"
                WorkManager.getInstance().cancelAllWorkByTag(workTag)
                val notificationWork = OneTimeWorkRequest.Builder(NotifyWorker::class.java)
                    .setInitialDelay(duration, TimeUnit.MINUTES)
                    .addTag(workTag)
                    .build()
                WorkManager.getInstance().enqueue(notificationWork)
            }

//            // Jam 6 Sore
//            val calendar2 = Calendar.getInstance()
//            calendar2.set(Calendar.SECOND, 0)
//            calendar2.set(Calendar.MINUTE, 5)
//            calendar2.set(Calendar.HOUR_OF_DAY, 6)
//            val duration2 = TimeUnit.MILLISECONDS.toMinutes((calendar2.timeInMillis - DateTimeUtils.waktuInLong))
//            if (duration2 > 0) {
//                val workTag = "LogoutReminderWork2"
//                WorkManager.getInstance().cancelAllWorkByTag(workTag)
//                val notificationWork2 = OneTimeWorkRequest.Builder(NotifyWorker::class.java)
//                    .setInitialDelay(duration2, TimeUnit.MINUTES)
//                    .addTag(workTag)
//                    .build()
//                WorkManager.getInstance().enqueue(notificationWork2)
//            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == Config.REQUEST_CODE_WRITE_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                StorageUtils.createDirectories()
                initDaoSession()
                DatabaseReport.getDatabase(this)
                val iService = Intent(applicationContext, ReportUploader::class.java)
                startService(iService)
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (currentFocus != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm!!.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}
