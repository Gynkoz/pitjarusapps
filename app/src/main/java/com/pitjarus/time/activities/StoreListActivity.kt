package com.pitjarus.time.activities

import android.Manifest
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.pitjarus.time.GlobalApp
import com.pitjarus.time.R
import com.pitjarus.time.adapter.CustomInfoWindowAdapter
import com.pitjarus.time.adapter.StoresAdapter
import com.pitjarus.time.database.DaoSession
import com.pitjarus.time.database.TStore
import com.pitjarus.time.database.TStoreDao
import com.pitjarus.time.models.*
import com.pitjarus.time.rests.ApiClient
import com.pitjarus.time.rests.LoginApiInterface
import com.pitjarus.time.utils.*
import kotlinx.android.synthetic.main.activity_store_list.*
import kotlinx.android.synthetic.main.content_dashboard.*
import org.greenrobot.greendao.query.WhereCondition
import org.joda.time.DateTime
import org.joda.time.LocalDate
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class StoreListActivity : AppCompatActivity(),
    OnMapReadyCallback,
    LocationListener,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    private val requestCodeTagLocation = 20

    private lateinit var daoSession: DaoSession
    private var mCurrentLocation: Location? = null
    private lateinit var mLocationManager: LocationManager
    private lateinit var mLocationRequest: LocationRequest
    private lateinit var mGoogleApiClient: GoogleApiClient
    private var mMap: GoogleMap? = null
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var recycler: RecyclerView
    private lateinit var storesAdapter: StoresAdapter
    private lateinit var storeViewModels: MutableList<StoreViewModel>
    private lateinit var mStores: List<TStore>
    private lateinit var mutStores: MutableList<Store>
    private lateinit var kaeStores: MutableList<StoreSku>
    private lateinit var mCurrentCuaca: Cuaca
    private lateinit var mNextCuaca: CuacaNext
    internal lateinit var mDrawer: MultiDirectionSlidingDrawer
    internal lateinit var mCurrentTimeWeather: TextView
    private lateinit var mBelumKunjungan: MutableList<TStore>
    private lateinit var mKunjungan: MutableList<TStore>

    private var radiusVerificationLimit: Int = 0
    private var progressDialog: ProgressDialog? = null
    private var mSurveyorId: Int = 0
    private var timeCurrent = 0
    private var timeNext = 0
    private var dateChoosedEnd=""
    private var dateChoosedStart=""
    private var mType = 0
    private var mItems: List<Store> = ArrayList<Store>()
    private var detik = ""
    private var menit = 0
    private var jam = 0
    private val cal = Calendar.getInstance()
    private var currentDate = ""
    private var currentMonth = ""
    private var currentYear = ""
    private var storeId = 0
    private var isBypass = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_list)

        setSupportActionBar(mtoolbar as Toolbar?)
        supportActionBar!!.setDisplayHomeAsUpEnabled(false)
        supportActionBar!!.setDisplayShowHomeEnabled(false)
        supportActionBar!!.setDisplayShowTitleEnabled(true)
        supportActionBar!!.title = getString(R.string.label_store_list)
        supportActionBar!!.subtitle = Helper.getSubtitle(this)

        if (!GooglePlayUtils.isGooglePlayServicesAvailable(this)) {
            finish()
            val appPackageName = getString(R.string.label_google_package_name)
            try {
                Toast.makeText(this, "Application need Google Play Services.\nGoogle Play Services not available or needs to be updated", Toast.LENGTH_LONG).show()
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("store://details?id=$appPackageName")))
            } catch (anfe: android.content.ActivityNotFoundException) {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
            }
        }

        createLocationRequest()
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
        mLocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        mapFragment = supportFragmentManager.findFragmentById(R.id.store_map) as SupportMapFragment
        recycler = findViewById(R.id.store_recycler)

        initData()
        initView()


    }

    private fun initData(){
        radiusVerificationLimit = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_RADIUS_VERIFICATION_LIMIT, 0)
        daoSession = (application as GlobalApp).daoSession!!
        progressDialog = ProgressDialog(this)
        mutStores = ArrayList()
        kaeStores = ArrayList()
        storeViewModels = ArrayList()
        mSurveyorId= Login.getUserId(this@StoreListActivity)
//        mStores = daoSession.tStoreDao.queryBuilder().where(TStoreDao.Properties.IsMandatory.eq(1)).list()
        mStores = daoSession.tStoreDao.queryBuilder().where(WhereCondition.StringCondition("1 GROUP BY store_id")).list()
        mCurrentCuaca = Cuaca()
        mNextCuaca = CuacaNext()
        dateChoosedEnd= LocalDate.now().toString("yyyy-MM-dd")
        dateChoosedStart= LocalDate.now().withDayOfMonth(1).toString("yyyy-MM-dd")
        mType = SharedPrefsUtils.getIntegerPreference(this, Config.KEY_EMPLOYEE_ROLE_ID,0)
        currentDate = DateTime.now().toString("dd")
        currentMonth = DateTime.now().toString("MM")
        currentYear = DateTime.now().toString("yyyy")

        isBypass = SharedPrefsUtils.getIntegerPreference(this@StoreListActivity, Config.KEY_TAGGING_LOCATION, 0)
        Log.i("taggi",isBypass.toString())


    }
    private fun initView(){
        mDrawer = findViewById(R.id.drawer) as MultiDirectionSlidingDrawer
        mCurrentTimeWeather = findViewById(R.id.currentTimeWeather) as TextView

        mDrawer.setOnDrawerOpenListener {
            if(mCurrentLocation?.longitude != null || mCurrentLocation?.latitude != null) {
                drawer.isClickable = true
                handle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_more_white_24dp)
                val currentDate = DateTime.now().toString(Config.DATE_FORMAT_DATABASE_WEATHER)
                mKunjungan = daoSession.tStoreDao.queryBuilder().where(TStoreDao.Properties.IsVisited.eq(1)).list()
                mBelumKunjungan = daoSession.tStoreDao.queryBuilder().where(TStoreDao.Properties.IsVisited.eq(0)).list()

                mCurrentTimeWeather.text = currentDate
                dashboard_lblTotalStore4.text = mStores.size.toString()
                dashboard_lblTotalStore5.text = mKunjungan.size.toString()
                dashboard_lblTotalStore6.text = mBelumKunjungan.size.toString()
                if(mType==2||mType==4||mType==5||mType==6) {
                    callGetKunjungi(mSurveyorId)
                }
                else{
                    callGetKunjungiDokter(mSurveyorId)
                }

                Log.i("TN",timeNext.toString())
                Log.i("TC",timeCurrent.toString())

                if (timeCurrent<1 || timeNext<1){
                    getWeather()
                    getNextWeather()
                }
                else if (cekSelisihWaktu2Jam()){
                    getWeather()
                    getNextWeather()

                }
                else{

                }

            }
            else{
                Toast.makeText(this@StoreListActivity, "Harap tunggu GPS sedang melakukan kalibrasi", Toast.LENGTH_SHORT).show();
            }


            mDrawer.setOnDrawerCloseListener {
                handle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_less_white_24dp)
                drawer.isClickable = false
            }
        }

        store_btnSearch.setOnClickListener {
                searchStores()
        }

}



    private fun getWeather() {
        progressBarCurrentWeather.visibility = View.VISIBLE
        val apiService = ApiClient.client2!!.create(LoginApiInterface::class.java)
        val call = apiService.getWeather(mCurrentLocation!!.latitude,mCurrentLocation!!.longitude,"6b13bed46ca166a0d45e4726089368c5","metric")
        call.enqueue(object : Callback<WeatherResponse> {
            override fun onResponse(call: Call<WeatherResponse>, response: Response<WeatherResponse>) {
                try {
                    if (response.isSuccessful) {
                        mCurrentCuaca.cuacaTemperatur = response.body().main!!.temp.toString()+"°C"
                        mCurrentCuaca.cuaca = response.body().weathers!!.get(0).info
                        mCurrentCuaca.timeHit = DateTime.now().toString(Config.DATE_FORMAT_DATABASE_WEATHER)
                        mCurrentCuaca.cuacaId = response.body().weathers!!.get(0).id
                        currentWeatherTemp.text = mCurrentCuaca.cuacaTemperatur
                        currentWeatherStatus.text = mCurrentCuaca.cuaca

                        Log.i("kutil1",mCurrentCuaca.cuacaTemperatur)
                        Log.i("kutil2",mCurrentCuaca.cuaca)
                        Log.i("kutil3",mCurrentCuaca.timeHit)

                        changePictureCurrentWeather()
                        timeCurrent=1

                    }
                    progressBarCurrentWeather.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<WeatherResponse>, t: Throwable) {
                try {
                    progressBarCurrentWeather.visibility = View.GONE
                    Toast.makeText(this@StoreListActivity, t.message, Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun getNextWeather() {
        progressBarNextWeather.visibility = View.VISIBLE
        val apiService = ApiClient.client2!!.create(LoginApiInterface::class.java)
        val call = apiService.getWeatherNext(mCurrentLocation!!.latitude,mCurrentLocation!!.longitude,"65ca8214adfddd5f749006df53827c4e","metric")
        call.enqueue(object : Callback<ListWeather> {
            override fun onResponse(call: Call<ListWeather>, response: Response<ListWeather>) {
                try {
                    if (response.isSuccessful) {

                        for (o in response.body().list!!) {

                            if(cekSelisihWaktu3Jam(o.dt_txt)) {
                                mNextCuaca.cuacaNext = o.weathers!!.get(0).info
                                mNextCuaca.cuacaTemperaturNext = o.main!!.temp.toString() + "°C"
                                mNextCuaca.timeHitNext = o.dt_txt
                                mNextCuaca.cuacaNextId = o.weathers!!.get(0).id
                                Log.i("kutil1",mNextCuaca.timeHitNext)
                                Log.i("kutil2",mNextCuaca.cuacaTemperaturNext)
                                Log.i("kutil3",mNextCuaca.cuacaNext)
                                break

                            }
                        }
                        predictWeatherTemp.text=mNextCuaca.cuacaTemperaturNext
                        predictWeatherStatus.text=mNextCuaca.cuacaNext

                        changePictureNextWeather()
                        timeNext=1
                    }
                    progressBarNextWeather.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<ListWeather>, t: Throwable) {
                try {
                    progressBarNextWeather.visibility = View.GONE
                    Toast.makeText(this@StoreListActivity, t.message, Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun cekSelisihWaktu3Jam(dt_txt: String): Boolean {
        val datevisitin = DateTime.now().toString(Config.DATE_FORMAT_DATABASE_WEATHER)
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val datevisitout = dt_txt
        Log.i("visitIn12",datevisitin)
        Log.i("visitOut12",datevisitout)
        try {
            val date1 = format.parse(datevisitin)
            val date2 = format.parse(datevisitout)
            val selisihwaktu = DateTimeUtils.SubtractingDate(date1, date2)
            //long selisihwaktu[] = {0,0,4};
            Log.i("selisih hari", selisihwaktu[0].toString())
            Log.i("selisih jam", selisihwaktu[1].toString())
            Log.i("selisih menit", selisihwaktu[2].toString())
            Log.i("date1", datevisitin)
            Log.i("date2", datevisitout)
            return if (selisihwaktu[0] > 0) {
                false
            } else {
                selisihwaktu[1] >= 3
            }//return true;
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        return true
    }

    private fun cekSelisihWaktu2Jam(): Boolean {
        val datevisitin = mCurrentCuaca.timeHit
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val datevisitout = DateTime.now().toString(Config.DATE_FORMAT_DATABASE_WEATHER)
        Log.i("visitin",datevisitin)
        Log.i("visitout",datevisitout)
        try {
            val date1 = format.parse(datevisitin)
            val date2 = format.parse(datevisitout)
            val selisihwaktu = DateTimeUtils.SubtractingDate(date1, date2)
            //long selisihwaktu[] = {0,0,4};
            Log.i("selisih hari", selisihwaktu[0].toString())
            Log.i("selisih jam", selisihwaktu[1].toString())
            Log.i("selisih menit", selisihwaktu[2].toString())
            Log.i("date1", date1.toString())
            Log.i("date2", date2.toString())
            return if (selisihwaktu[0] >= 1) {
                true
            } else {
                selisihwaktu[1] >= 2

            }//return true;

        } catch (e: ParseException) {
            e.printStackTrace()
        }
        Log.i("mega","ulol4")
        return true
    }

    private fun changePictureCurrentWeather(){

        if(mCurrentCuaca.cuacaId >= 801 && mCurrentCuaca.cuacaId <=820){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_berawan))
            currentWeatherStatus.text="Berawan"
        }
        else if(mCurrentCuaca.cuacaId >= 500 && mCurrentCuaca.cuacaId <=540){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_hujan))
            currentWeatherStatus.text="Hujan"
        }
        else if(mCurrentCuaca.cuacaId == 800){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_cerah))
            currentWeatherStatus.text="Cerah"
        }
        else if(mCurrentCuaca.cuacaId >= 300 && mCurrentCuaca.cuacaId <=330){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_gerimis))
            currentWeatherStatus.text="Gerimis"
        }
        else if(mCurrentCuaca.cuacaId >= 200 && mCurrentCuaca.cuacaId <=240){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_storm))
            currentWeatherStatus.text="Badai"
        }
        else if(mCurrentCuaca.cuacaId >= 701 && mCurrentCuaca.cuacaId <=790){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_haze))
            currentWeatherStatus.text="Kabut"
        }
        else if(mCurrentCuaca.cuacaId >= 600 && mCurrentCuaca.cuacaId <=630){
            currentWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_hujan))
            currentWeatherStatus.text="Hujan"
        }
    }

    private fun BypassStoreList() {
//            store_lblLoading.visibility = View.VISIBLE
//            checkGPSPermission(requestCodeTagLocation)
        stopLocationUpdates()
//                mIgnoreGps = true

        storeViewModels = ArrayList()
        for (o in mStores) {
            val storeViewModel = StoreViewModel()
            storeViewModel.store = o
            storeViewModel.distance = 0f
            storeViewModels.add(storeViewModel)
        }

        val list = storeViewModels
            .filter { it.store!!.storeName.toLowerCase().contains(store_txtSearch.text.toString().toLowerCase()) }
            .sortedBy { it.distance }
        storesAdapter = StoresAdapter(storeViewModels, R.layout.activity_store_list_row, radiusVerificationLimit, object : StoresAdapter.OnItemClickListener {
            override fun onItemClick(item: StoreViewModel) {
                if(mType==3) {
                    Log.i("logisvisit",item.store!!.isStoreVisit.toString())
                    if (item.store!!.latitude == 0.toDouble() && item.store!!.longitude == 0.toDouble()) {
                        val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                        intent.putExtra("StoreId", item.store!!.storeId)
                        intent.putExtra("locationStatus", 0)
                        startActivity(intent)
                    } else {
                        if (item.distance <= radiusVerificationLimit) {
                            val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                            intent.putExtra("StoreId", item.store!!.storeId)
                            intent.putExtra("locationStatus", 1)
                            startActivity(intent)
                        }
                        else {
                            val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                            intent.putExtra("StoreId", item.store!!.storeId)
                            intent.putExtra("locationStatus", 0)
                            startActivity(intent)
                        }
                    }
                }else {
                    if (item.store!!.isVisited == Config.NO_CODE) {
                        if (item.store!!.latitude == 0.toDouble() && item.store!!.longitude == 0.toDouble()) {
                            val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                            intent.putExtra("StoreId", item.store!!.storeId)
                            intent.putExtra("locationStatus", 0)
                            startActivity(intent)
                        } else {
                            if (item.distance <= radiusVerificationLimit) {
                                val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                intent.putExtra("StoreId", item.store!!.storeId)
                                intent.putExtra("locationStatus", 1)
                                startActivity(intent)
                            } else {
                                val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                intent.putExtra("StoreId", item.store!!.storeId)
                                intent.putExtra("locationStatus", 0)
                                startActivity(intent)
                            }
                        }
                    } else {
                        Toast.makeText(
                            this@StoreListActivity,
                            item.store!!.storeName + " sudah Anda kunjungi",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        })

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = storesAdapter
        storesAdapter.notifyDataSetChanged()
    }


    private fun changePictureNextWeather(){

        if(mNextCuaca.cuacaNextId >= 801 && mNextCuaca.cuacaNextId <=820){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_berawan))
            predictWeatherStatus.text="Berawan"
        }
        else if(mNextCuaca.cuacaNextId >= 500 && mNextCuaca.cuacaNextId <=540){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_hujan))
            predictWeatherStatus.text="Hujan"
        }
        else if(mNextCuaca.cuacaNextId == 800){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_cerah))
            predictWeatherStatus.text="Cerah"
        }
        else if(mNextCuaca.cuacaNextId >= 300 && mNextCuaca.cuacaNextId <=330){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_gerimis))
            predictWeatherStatus.text="Gerimis"
        }
        else if(mNextCuaca.cuacaNextId >= 200 && mNextCuaca.cuacaNextId <=240){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_storm))
            predictWeatherStatus.text="Badai"
        }
        else if(mNextCuaca.cuacaNextId >= 701 && mNextCuaca.cuacaNextId <=790){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_haze))
            predictWeatherStatus.text="Kabut"
        }
        else if(mNextCuaca.cuacaNextId >= 600 && mNextCuaca.cuacaNextId <=630){
            predictWeatherImage.setBackgroundDrawable(ContextCompat.getDrawable(this,R.drawable.ic_weather_hujan))
            predictWeatherStatus.text="Hujan"
        }
    }

    private fun callGetKunjungi(userId: Int){

        progressBarKunjungan1.visibility = View.VISIBLE
        progressBarKunjungan2.visibility = View.VISIBLE
        progressBarKunjungan3.visibility = View.VISIBLE
        progressBarKunjungan4.visibility = View.VISIBLE
        progressBarActual.visibility = View.VISIBLE
        progressBarPercentage.visibility = View.VISIBLE
        progressBarTarget.visibility = View.VISIBLE

        val apiService = ApiClient.client!!.create(LoginApiInterface::class.java)
        val call = apiService.getKunjunganUser(userId,dateChoosedStart,dateChoosedEnd)
        call.enqueue(object : Callback<KunjunganUser> {
            override fun onResponse(call: Call<KunjunganUser>, response: Response<KunjunganUser>) {
                try {
                    if (response.isSuccessful) {
                        if (response.body().status == Config.STATUS_SUCCESS) {
                            dashboard_lblTotalStore7.text = "${response.body().actualVisit}"
                            dashboard_lblTotalTarget.text = "${response.body().target}"
                            dashboard_lblTotalActual.text = "${response.body().actual}"
                            dashboard_lblTotalPercentage.text = "${response.body().percentage}%"

                        } else if (response.body().status == Config.STATUS_FAILURE) {
                            Toast.makeText(this@StoreListActivity, response.body().message, Toast.LENGTH_SHORT).show()
                        }
                    }
                    progressBarKunjungan1.visibility = View.GONE
                    progressBarKunjungan2.visibility = View.GONE
                    progressBarKunjungan3.visibility = View.GONE
                    progressBarKunjungan4.visibility = View.GONE
                    progressBarActual.visibility = View.GONE
                    progressBarPercentage.visibility = View.GONE
                    progressBarTarget.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<KunjunganUser>, t: Throwable) {
                try {
                    progressBarKunjungan1.visibility = View.GONE
                    progressBarKunjungan2.visibility = View.GONE
                    progressBarKunjungan3.visibility = View.GONE
                    progressBarKunjungan4.visibility = View.GONE
                    Toast.makeText(this@StoreListActivity, t.message, Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun callGetKunjungiDokter(userId: Int){

        progressBarKunjungan1.visibility = View.VISIBLE
        progressBarKunjungan2.visibility = View.VISIBLE
        progressBarKunjungan3.visibility = View.VISIBLE
        progressBarKunjungan4.visibility = View.VISIBLE
        progressBarActual.visibility = View.VISIBLE
        progressBarPercentage.visibility = View.VISIBLE
        progressBarTarget.visibility = View.VISIBLE

        val apiService = ApiClient.client!!.create(LoginApiInterface::class.java)
        val call = apiService.getKunjunganUserDokter(userId,dateChoosedStart,dateChoosedEnd)
        call.enqueue(object : Callback<KunjunganUser> {
            override fun onResponse(call: Call<KunjunganUser>, response: Response<KunjunganUser>) {
                try {
                    if (response.isSuccessful) {
                        if (response.body().status == Config.STATUS_SUCCESS) {
                            dashboard_lblTotalStore7.text = "${response.body().actualVisit}"
                            dashboard_lblTotalTarget.text = "${response.body().target}"
                            dashboard_lblTotalActual.text = "${response.body().actual}"
                            dashboard_lblTotalPercentage.text = "${response.body().percentage}%"

                        } else if (response.body().status == Config.STATUS_FAILURE) {
                            Toast.makeText(this@StoreListActivity, response.body().message, Toast.LENGTH_SHORT).show()
                        }
                    }
                    progressBarKunjungan1.visibility = View.GONE
                    progressBarKunjungan2.visibility = View.GONE
                    progressBarKunjungan3.visibility = View.GONE
                    progressBarKunjungan4.visibility = View.GONE
                    progressBarActual.visibility = View.GONE
                    progressBarPercentage.visibility = View.GONE
                    progressBarTarget.visibility = View.GONE
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onFailure(call: Call<KunjunganUser>, t: Throwable) {
                try {
                    progressBarKunjungan1.visibility = View.GONE
                    progressBarKunjungan2.visibility = View.GONE
                    progressBarKunjungan3.visibility = View.GONE
                    progressBarKunjungan4.visibility = View.GONE
                    Toast.makeText(this@StoreListActivity, t.message, Toast.LENGTH_SHORT).show()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        })
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = Config.INTERVAL
        mLocationRequest.fastestInterval = Config.FASTEST_INTERVAL
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun stopLocationUpdates() {
        if (mGoogleApiClient.isConnected) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this)
            window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            store_lblLoading.visibility = View.GONE
            store_lblLoading.text = getString(R.string.label_mencari_lokasi)
        }
    }

    override fun onLocationChanged(location: Location) {
//        if(isBypass == 1){
            if (LocationUtils.isBetterLocation(location, mCurrentLocation)) {
                mCurrentLocation = location
                if (LocationUtils.isFromMockProvider(this, location)) {
                    stopLocationUpdates()
                    window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    Toast.makeText(this, "Anda terdeteksi menggunakan aplikasi yang bekerja memanipulasi lokasi, harap dinonaktifkan!", Toast.LENGTH_LONG).show()
                } else {
                    if (location.accuracy <= Config.ACCURACY_LIMIT) {
                        stopLocationUpdates()
                        mCurrentLocation = location
                        mapFragment.getMapAsync(this)
                    }
                }
            }
//        }
//        else{
//            BypassStoreList()
//        }

    }

    override fun onMapReady(map: GoogleMap) {
        if (mMap != null) {
            mMap!!.clear()
        }
        mMap = map
        val peoplePoint = LatLng(mCurrentLocation!!.latitude, mCurrentLocation!!.longitude)
        var storeLocation: Location

        storeViewModels = ArrayList()
        for (o in mStores) {
            storeLocation = Location("StoreLocation")
            storeLocation.latitude = o.latitude
            storeLocation.longitude = o.longitude
            val distance = mCurrentLocation!!.distanceTo(storeLocation)
            val pinId: Float
            pinId = when {
                distance <= radiusVerificationLimit - 20 -> BitmapDescriptorFactory.HUE_GREEN
                distance <= radiusVerificationLimit -> BitmapDescriptorFactory.HUE_YELLOW
                else -> BitmapDescriptorFactory.HUE_RED
            }

            mMap!!.addMarker(
                MarkerOptions()
                    .position(LatLng(o.latitude, o.longitude))
                    .title(o.storeName)
                    .icon(BitmapDescriptorFactory.defaultMarker(pinId))
                    .snippet("${o.storeId};${o.storeCode};${o.address};${o.territoryName};$distance")
            )
            val storeViewModel = StoreViewModel()
            storeViewModel.store = o
            storeViewModel.distance = distance
            storeViewModels.add(storeViewModel)
        }

//        var storesssss = daoSession.tStoreDao.queryBuilder().where(TStoreDao.Properties.IsMandatory.eq(0)).limit(1).unique()
//
//        if(storesssss!=null) {
//            val storeViewModel = StoreViewModel()
//            storeLocation = Location("StoreLocation")
//            storeLocation.latitude = storesssss.latitude
//            storeLocation.longitude = storesssss.longitude
//            val distance = mCurrentLocation!!.distanceTo(storeLocation)
//
//            storeViewModel.store = storesssss
//            storeViewModel.distance = distance
//            storeViewModels.add(0, storeViewModel)
//        }
        storesAdapter = StoresAdapter(storeViewModels, R.layout.activity_store_list_row, radiusVerificationLimit, object : StoresAdapter.OnItemClickListener {
            override fun onItemClick(item: StoreViewModel) {
                if(mType==3) {
                    Log.i("logisvisit",item.store!!.isStoreVisit.toString())
                    if (item.store!!.latitude == 0.toDouble() && item.store!!.longitude == 0.toDouble()) {
                        val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                        intent.putExtra("StoreId", item.store!!.storeId)
                        intent.putExtra("locationStatus", 0)
                        startActivity(intent)
                    } else {
                        if (item.distance <= radiusVerificationLimit) {
                            val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                            intent.putExtra("StoreId", item.store!!.storeId)
                            intent.putExtra("locationStatus", 1)
                            startActivity(intent)
                        }
                        else {
                            val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                            intent.putExtra("StoreId", item.store!!.storeId)
                            intent.putExtra("locationStatus", 0)
                            startActivity(intent)
                        }
                    }
                }else {
                    if (item.store!!.isVisited == Config.NO_CODE) {
                        if (item.store!!.latitude == 0.toDouble() && item.store!!.longitude == 0.toDouble()) {
                            val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                            intent.putExtra("StoreId", item.store!!.storeId)
                            intent.putExtra("locationStatus", 0)
                            startActivity(intent)
                        } else {
                            if (item.distance <= radiusVerificationLimit) {
                                val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                intent.putExtra("StoreId", item.store!!.storeId)
                                intent.putExtra("locationStatus", 1)
                                startActivity(intent)
                            } else {
                                val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                intent.putExtra("StoreId", item.store!!.storeId)
                                intent.putExtra("locationStatus", 0)
                                startActivity(intent)
                            }
                        }
                    } else {
                        Toast.makeText(
                            this@StoreListActivity,
                            item.store!!.storeName + " sudah Anda kunjungi",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        })
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = storesAdapter
        storesAdapter.notifyDataSetChanged()

        mMap!!.addMarker(
            MarkerOptions()
                .position(peoplePoint)
                .title(Login.getEmployeeName(this))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_people_pin_36px))
                .snippet(""))

        mMap!!.addCircle(
            CircleOptions()
                .center(peoplePoint)
                .radius(radiusVerificationLimit.toDouble())
                .fillColor(resources.getColor(R.color.transparent_blue))
                .strokeColor(resources.getColor(R.color.piction_blue))
                .strokeWidth(5f))

        mMap!!.moveCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.Builder()
                    .target(peoplePoint)
                    .zoom(15f)
                    .bearing(0f)
                    .tilt(40f)
                    .build()))

        mMap!!.setInfoWindowAdapter(CustomInfoWindowAdapter(this))
    }

    public override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
        stopLocationUpdates()
        mGoogleApiClient.disconnect()
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    override fun onConnected(bundle: Bundle?) {
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onConnectionSuspended(i: Int) {}

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    private fun checkGPSPermission(requestCode: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                    LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
                } else {
                    showGPSSetting()
                }
            } else {
                if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    Toast.makeText(this, "Location permission is needed to get current location.", Toast.LENGTH_SHORT).show()
                }
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), requestCode)
            }
        } else {
            if (mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)
            } else {
                showGPSSetting()
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == requestCodeTagLocation) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkGPSPermission(requestCodeTagLocation)
            } else {
                Toast.makeText(this, "Permission was not granted", Toast.LENGTH_SHORT).show()
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }

    private fun showGPSSetting() {
        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            val builder = AlertDialog.Builder(this)
            builder.setTitle(getString(R.string.label_gps_not_available))
            builder.setMessage(getString(R.string.info_msg_required_gps))
            builder.setPositiveButton(getString(R.string.label_gps_setting)) { _, _ ->
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
            builder.setNegativeButton(getString(R.string.label_cancel)) { dialog, _ -> dialog.dismiss() }
            val alertDialog = builder.create()
            alertDialog.setCanceledOnTouchOutside(false)
            alertDialog.show()
        }
    }

    private fun searchStores() {
        if (mCurrentLocation != null) {
            if (storeViewModels.isNotEmpty()) {
                val list = storeViewModels
                    .filter { it.store!!.storeName.toLowerCase().contains(store_txtSearch.text.toString().toLowerCase()) }
                    .sortedBy { it.distance }
                storesAdapter = StoresAdapter(list, R.layout.activity_store_list_row, radiusVerificationLimit,object : StoresAdapter.OnItemClickListener {
                    override fun onItemClick(item: StoreViewModel) {
                        if(mType==3) {
                            Log.i("logisvisit",item.store!!.isStoreVisit.toString())
                            if (item.store!!.latitude == 0.toDouble() && item.store!!.longitude == 0.toDouble()) {
                                val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                intent.putExtra("StoreId", item.store!!.storeId)
                                intent.putExtra("locationStatus", 0)
                                startActivity(intent)
                            } else {
                                if (item.distance <= radiusVerificationLimit) {
                                    val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                    intent.putExtra("StoreId", item.store!!.storeId)
                                    intent.putExtra("locationStatus", 1)
                                    startActivity(intent)
                                }
                                else {
                                    val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                    intent.putExtra("StoreId", item.store!!.storeId)
                                    intent.putExtra("locationStatus", 0)
                                    startActivity(intent)
                                }
                            }
                        }else {
                            if (item.store!!.isVisited == Config.NO_CODE) {
                                if (item.store!!.latitude == 0.toDouble() && item.store!!.longitude == 0.toDouble()) {
                                    val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                    intent.putExtra("StoreId", item.store!!.storeId)
                                    intent.putExtra("locationStatus", 0)
                                    startActivity(intent)
                                } else {
                                    if (item.distance <= radiusVerificationLimit) {
                                        val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                        intent.putExtra("StoreId", item.store!!.storeId)
                                        intent.putExtra("locationStatus", 1)
                                        startActivity(intent)
                                    } else {
                                        val intent = Intent(this@StoreListActivity, StoreDetailActivity::class.java)
                                        intent.putExtra("StoreId", item.store!!.storeId)
                                        intent.putExtra("locationStatus", 0)
                                        startActivity(intent)
                                    }
                                }
                            }
                        else {
                            Toast.makeText(this@StoreListActivity, item.store!!.storeName + " sudah Anda kunjungi", Toast.LENGTH_SHORT).show();
                        }
                            }
                    }
                })
                recycler.layoutManager = LinearLayoutManager(this)
                recycler.adapter = storesAdapter
                storesAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun isValidForm(): Boolean {
//        for(c in mStores) {
////            if(c.isReporting==1) {
//                if(c.isMandatory == 1) {
//                    if (c.isVisited != 1) {
//                        return false
//                    }
//                }
////            }
//        }
        return true
    }

    override fun onBackPressed() {}

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_store_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_logout) {
            if(isValidForm()){
                val builder = android.app.AlertDialog.Builder(this)
                builder.setTitle(getString(R.string.label_confirm))
                builder.setMessage(getString(R.string.confirm_msg_logout))
                builder.setPositiveButton(getString(R.string.label_yes)) { _, _ ->  }
                builder.setNegativeButton(getString(R.string.label_no)) { dialog, _ -> dialog.dismiss() }
                val alertDialog = builder.create()
                alertDialog.setCanceledOnTouchOutside(false)
                alertDialog.show()
            }
            else{
                Toast.makeText(this, "Anda harus mengunjungi semua toko", Toast.LENGTH_SHORT).show()
            }
        }

        if (id == R.id.action_refresh) {
        }

        return super.onOptionsItemSelected(item)
    }
}
