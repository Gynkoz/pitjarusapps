package com.pitjarus.time.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "TSURVEY_COMPETITOR_PRODUCT_ANSWER".
*/
public class TSurveyCompetitorProductAnswerDao extends AbstractDao<TSurveyCompetitorProductAnswer, Long> {

    public static final String TABLENAME = "TSURVEY_COMPETITOR_PRODUCT_ANSWER";

    /**
     * Properties of entity TSurveyCompetitorProductAnswer.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property ReportId = new Property(1, String.class, "ReportId", false, "REPORT_ID");
        public final static Property UserId = new Property(2, Integer.class, "UserId", false, "USER_ID");
        public final static Property CustomerGroupId = new Property(3, Integer.class, "CustomerGroupId", false, "CUSTOMER_GROUP_ID");
        public final static Property CompetitorProductId = new Property(4, Integer.class, "CompetitorProductId", false, "COMPETITOR_PRODUCT_ID");
        public final static Property CompetitorProductQuestion = new Property(5, String.class, "CompetitorProductQuestion", false, "COMPETITOR_PRODUCT_QUESTION");
        public final static Property CompetitorProductAnswerType = new Property(6, Integer.class, "CompetitorProductAnswerType", false, "COMPETITOR_PRODUCT_ANSWER_TYPE");
        public final static Property VisitId = new Property(7, String.class, "VisitId", false, "VISIT_ID");
        public final static Property ReasonId = new Property(8, Integer.class, "ReasonId", false, "REASON_ID");
        public final static Property ReasonName = new Property(9, String.class, "ReasonName", false, "REASON_NAME");
        public final static Property IsChecked = new Property(10, Integer.class, "IsChecked", false, "IS_CHECKED");
    }


    public TSurveyCompetitorProductAnswerDao(DaoConfig config) {
        super(config);
    }
    
    public TSurveyCompetitorProductAnswerDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"TSURVEY_COMPETITOR_PRODUCT_ANSWER\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"REPORT_ID\" TEXT," + // 1: ReportId
                "\"USER_ID\" INTEGER," + // 2: UserId
                "\"CUSTOMER_GROUP_ID\" INTEGER," + // 3: CustomerGroupId
                "\"COMPETITOR_PRODUCT_ID\" INTEGER," + // 4: CompetitorProductId
                "\"COMPETITOR_PRODUCT_QUESTION\" TEXT," + // 5: CompetitorProductQuestion
                "\"COMPETITOR_PRODUCT_ANSWER_TYPE\" INTEGER," + // 6: CompetitorProductAnswerType
                "\"VISIT_ID\" TEXT," + // 7: VisitId
                "\"REASON_ID\" INTEGER," + // 8: ReasonId
                "\"REASON_NAME\" TEXT," + // 9: ReasonName
                "\"IS_CHECKED\" INTEGER);"); // 10: IsChecked
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"TSURVEY_COMPETITOR_PRODUCT_ANSWER\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, TSurveyCompetitorProductAnswer entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String ReportId = entity.getReportId();
        if (ReportId != null) {
            stmt.bindString(2, ReportId);
        }
 
        Integer UserId = entity.getUserId();
        if (UserId != null) {
            stmt.bindLong(3, UserId);
        }
 
        Integer CustomerGroupId = entity.getCustomerGroupId();
        if (CustomerGroupId != null) {
            stmt.bindLong(4, CustomerGroupId);
        }
 
        Integer CompetitorProductId = entity.getCompetitorProductId();
        if (CompetitorProductId != null) {
            stmt.bindLong(5, CompetitorProductId);
        }
 
        String CompetitorProductQuestion = entity.getCompetitorProductQuestion();
        if (CompetitorProductQuestion != null) {
            stmt.bindString(6, CompetitorProductQuestion);
        }
 
        Integer CompetitorProductAnswerType = entity.getCompetitorProductAnswerType();
        if (CompetitorProductAnswerType != null) {
            stmt.bindLong(7, CompetitorProductAnswerType);
        }
 
        String VisitId = entity.getVisitId();
        if (VisitId != null) {
            stmt.bindString(8, VisitId);
        }
 
        Integer ReasonId = entity.getReasonId();
        if (ReasonId != null) {
            stmt.bindLong(9, ReasonId);
        }
 
        String ReasonName = entity.getReasonName();
        if (ReasonName != null) {
            stmt.bindString(10, ReasonName);
        }
 
        Integer IsChecked = entity.getIsChecked();
        if (IsChecked != null) {
            stmt.bindLong(11, IsChecked);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, TSurveyCompetitorProductAnswer entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        String ReportId = entity.getReportId();
        if (ReportId != null) {
            stmt.bindString(2, ReportId);
        }
 
        Integer UserId = entity.getUserId();
        if (UserId != null) {
            stmt.bindLong(3, UserId);
        }
 
        Integer CustomerGroupId = entity.getCustomerGroupId();
        if (CustomerGroupId != null) {
            stmt.bindLong(4, CustomerGroupId);
        }
 
        Integer CompetitorProductId = entity.getCompetitorProductId();
        if (CompetitorProductId != null) {
            stmt.bindLong(5, CompetitorProductId);
        }
 
        String CompetitorProductQuestion = entity.getCompetitorProductQuestion();
        if (CompetitorProductQuestion != null) {
            stmt.bindString(6, CompetitorProductQuestion);
        }
 
        Integer CompetitorProductAnswerType = entity.getCompetitorProductAnswerType();
        if (CompetitorProductAnswerType != null) {
            stmt.bindLong(7, CompetitorProductAnswerType);
        }
 
        String VisitId = entity.getVisitId();
        if (VisitId != null) {
            stmt.bindString(8, VisitId);
        }
 
        Integer ReasonId = entity.getReasonId();
        if (ReasonId != null) {
            stmt.bindLong(9, ReasonId);
        }
 
        String ReasonName = entity.getReasonName();
        if (ReasonName != null) {
            stmt.bindString(10, ReasonName);
        }
 
        Integer IsChecked = entity.getIsChecked();
        if (IsChecked != null) {
            stmt.bindLong(11, IsChecked);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public TSurveyCompetitorProductAnswer readEntity(Cursor cursor, int offset) {
        TSurveyCompetitorProductAnswer entity = new TSurveyCompetitorProductAnswer( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1), // ReportId
            cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2), // UserId
            cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3), // CustomerGroupId
            cursor.isNull(offset + 4) ? null : cursor.getInt(offset + 4), // CompetitorProductId
            cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5), // CompetitorProductQuestion
            cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6), // CompetitorProductAnswerType
            cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7), // VisitId
            cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8), // ReasonId
            cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9), // ReasonName
            cursor.isNull(offset + 10) ? null : cursor.getInt(offset + 10) // IsChecked
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, TSurveyCompetitorProductAnswer entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setReportId(cursor.isNull(offset + 1) ? null : cursor.getString(offset + 1));
        entity.setUserId(cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2));
        entity.setCustomerGroupId(cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3));
        entity.setCompetitorProductId(cursor.isNull(offset + 4) ? null : cursor.getInt(offset + 4));
        entity.setCompetitorProductQuestion(cursor.isNull(offset + 5) ? null : cursor.getString(offset + 5));
        entity.setCompetitorProductAnswerType(cursor.isNull(offset + 6) ? null : cursor.getInt(offset + 6));
        entity.setVisitId(cursor.isNull(offset + 7) ? null : cursor.getString(offset + 7));
        entity.setReasonId(cursor.isNull(offset + 8) ? null : cursor.getInt(offset + 8));
        entity.setReasonName(cursor.isNull(offset + 9) ? null : cursor.getString(offset + 9));
        entity.setIsChecked(cursor.isNull(offset + 10) ? null : cursor.getInt(offset + 10));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(TSurveyCompetitorProductAnswer entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(TSurveyCompetitorProductAnswer entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(TSurveyCompetitorProductAnswer entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
