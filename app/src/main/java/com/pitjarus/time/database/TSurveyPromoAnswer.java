package com.pitjarus.time.database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TSURVEY_PROMO_ANSWER".
 */
@Entity
public class TSurveyPromoAnswer {

    @Id
    private Long id;
    private Integer UserId;
    private Integer PromoId;
    private String PromoQuestion;
    private Integer PromoAnswerType;
    private String VisitId;
    private Integer IsChecked;
    private Integer ReasonId;
    private String ReasonName;

    @Generated
    public TSurveyPromoAnswer() {
    }

    public TSurveyPromoAnswer(Long id) {
        this.id = id;
    }

    @Generated
    public TSurveyPromoAnswer(Long id, Integer UserId, Integer PromoId, String PromoQuestion, Integer PromoAnswerType, String VisitId, Integer IsChecked, Integer ReasonId, String ReasonName) {
        this.id = id;
        this.UserId = UserId;
        this.PromoId = PromoId;
        this.PromoQuestion = PromoQuestion;
        this.PromoAnswerType = PromoAnswerType;
        this.VisitId = VisitId;
        this.IsChecked = IsChecked;
        this.ReasonId = ReasonId;
        this.ReasonName = ReasonName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer UserId) {
        this.UserId = UserId;
    }

    public Integer getPromoId() {
        return PromoId;
    }

    public void setPromoId(Integer PromoId) {
        this.PromoId = PromoId;
    }

    public String getPromoQuestion() {
        return PromoQuestion;
    }

    public void setPromoQuestion(String PromoQuestion) {
        this.PromoQuestion = PromoQuestion;
    }

    public Integer getPromoAnswerType() {
        return PromoAnswerType;
    }

    public void setPromoAnswerType(Integer PromoAnswerType) {
        this.PromoAnswerType = PromoAnswerType;
    }

    public String getVisitId() {
        return VisitId;
    }

    public void setVisitId(String VisitId) {
        this.VisitId = VisitId;
    }

    public Integer getIsChecked() {
        return IsChecked;
    }

    public void setIsChecked(Integer IsChecked) {
        this.IsChecked = IsChecked;
    }

    public Integer getReasonId() {
        return ReasonId;
    }

    public void setReasonId(Integer ReasonId) {
        this.ReasonId = ReasonId;
    }

    public String getReasonName() {
        return ReasonName;
    }

    public void setReasonName(String ReasonName) {
        this.ReasonName = ReasonName;
    }

}
