package com.pitjarus.time.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "TSURVEY_COMPETITOR_PRODUCT".
*/
public class TSurveyCompetitorProductDao extends AbstractDao<TSurveyCompetitorProduct, Long> {

    public static final String TABLENAME = "TSURVEY_COMPETITOR_PRODUCT";

    /**
     * Properties of entity TSurveyCompetitorProduct.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property UserId = new Property(1, Integer.class, "UserId", false, "USER_ID");
        public final static Property CustomerGroupId = new Property(2, Integer.class, "CustomerGroupId", false, "CUSTOMER_GROUP_ID");
        public final static Property CompetitorProductId = new Property(3, Integer.class, "CompetitorProductId", false, "COMPETITOR_PRODUCT_ID");
        public final static Property CompetitorProductQuestion = new Property(4, String.class, "CompetitorProductQuestion", false, "COMPETITOR_PRODUCT_QUESTION");
        public final static Property CompetitorProductAnswerType = new Property(5, Integer.class, "CompetitorProductAnswerType", false, "COMPETITOR_PRODUCT_ANSWER_TYPE");
        public final static Property VisitId = new Property(6, String.class, "VisitId", false, "VISIT_ID");
    }


    public TSurveyCompetitorProductDao(DaoConfig config) {
        super(config);
    }
    
    public TSurveyCompetitorProductDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"TSURVEY_COMPETITOR_PRODUCT\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"USER_ID\" INTEGER," + // 1: UserId
                "\"CUSTOMER_GROUP_ID\" INTEGER," + // 2: CustomerGroupId
                "\"COMPETITOR_PRODUCT_ID\" INTEGER," + // 3: CompetitorProductId
                "\"COMPETITOR_PRODUCT_QUESTION\" TEXT," + // 4: CompetitorProductQuestion
                "\"COMPETITOR_PRODUCT_ANSWER_TYPE\" INTEGER," + // 5: CompetitorProductAnswerType
                "\"VISIT_ID\" TEXT);"); // 6: VisitId
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"TSURVEY_COMPETITOR_PRODUCT\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, TSurveyCompetitorProduct entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer UserId = entity.getUserId();
        if (UserId != null) {
            stmt.bindLong(2, UserId);
        }
 
        Integer CustomerGroupId = entity.getCustomerGroupId();
        if (CustomerGroupId != null) {
            stmt.bindLong(3, CustomerGroupId);
        }
 
        Integer CompetitorProductId = entity.getCompetitorProductId();
        if (CompetitorProductId != null) {
            stmt.bindLong(4, CompetitorProductId);
        }
 
        String CompetitorProductQuestion = entity.getCompetitorProductQuestion();
        if (CompetitorProductQuestion != null) {
            stmt.bindString(5, CompetitorProductQuestion);
        }
 
        Integer CompetitorProductAnswerType = entity.getCompetitorProductAnswerType();
        if (CompetitorProductAnswerType != null) {
            stmt.bindLong(6, CompetitorProductAnswerType);
        }
 
        String VisitId = entity.getVisitId();
        if (VisitId != null) {
            stmt.bindString(7, VisitId);
        }
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, TSurveyCompetitorProduct entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer UserId = entity.getUserId();
        if (UserId != null) {
            stmt.bindLong(2, UserId);
        }
 
        Integer CustomerGroupId = entity.getCustomerGroupId();
        if (CustomerGroupId != null) {
            stmt.bindLong(3, CustomerGroupId);
        }
 
        Integer CompetitorProductId = entity.getCompetitorProductId();
        if (CompetitorProductId != null) {
            stmt.bindLong(4, CompetitorProductId);
        }
 
        String CompetitorProductQuestion = entity.getCompetitorProductQuestion();
        if (CompetitorProductQuestion != null) {
            stmt.bindString(5, CompetitorProductQuestion);
        }
 
        Integer CompetitorProductAnswerType = entity.getCompetitorProductAnswerType();
        if (CompetitorProductAnswerType != null) {
            stmt.bindLong(6, CompetitorProductAnswerType);
        }
 
        String VisitId = entity.getVisitId();
        if (VisitId != null) {
            stmt.bindString(7, VisitId);
        }
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public TSurveyCompetitorProduct readEntity(Cursor cursor, int offset) {
        TSurveyCompetitorProduct entity = new TSurveyCompetitorProduct( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1), // UserId
            cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2), // CustomerGroupId
            cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3), // CompetitorProductId
            cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4), // CompetitorProductQuestion
            cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5), // CompetitorProductAnswerType
            cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6) // VisitId
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, TSurveyCompetitorProduct entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setUserId(cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1));
        entity.setCustomerGroupId(cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2));
        entity.setCompetitorProductId(cursor.isNull(offset + 3) ? null : cursor.getInt(offset + 3));
        entity.setCompetitorProductQuestion(cursor.isNull(offset + 4) ? null : cursor.getString(offset + 4));
        entity.setCompetitorProductAnswerType(cursor.isNull(offset + 5) ? null : cursor.getInt(offset + 5));
        entity.setVisitId(cursor.isNull(offset + 6) ? null : cursor.getString(offset + 6));
     }
    
    @Override
    protected final Long updateKeyAfterInsert(TSurveyCompetitorProduct entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(TSurveyCompetitorProduct entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(TSurveyCompetitorProduct entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
