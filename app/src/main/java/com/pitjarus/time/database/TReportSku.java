package com.pitjarus.time.database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TREPORT_SKU".
 */
@Entity
public class TReportSku {

    @Id
    private Long id;

    @NotNull
    private String ReportId;

    @NotNull
    private String VisitId;

    @NotNull
    private String HeaderId;
    private int StoreId;
    private int UserId;
    private int SkuId;

    @NotNull
    private String SkuCode;

    @NotNull
    private String SkuName;
    private int IsIr;
    private int CustomerGroupId;
    private int BrandId;

    @NotNull
    private String BrandName;
    private String ImagePath;
    private int Quantity;
    private int Depth;
    private Integer IsCompetitor;
    private boolean IsDone;
    private boolean IsMedrep;

    @Generated
    public TReportSku() {
    }

    public TReportSku(Long id) {
        this.id = id;
    }

    @Generated
    public TReportSku(Long id, String ReportId, String VisitId, String HeaderId, int StoreId, int UserId, int SkuId, String SkuCode, String SkuName, int IsIr, int CustomerGroupId, int BrandId, String BrandName, String ImagePath, int Quantity, int Depth, Integer IsCompetitor, boolean IsDone, boolean IsMedrep) {
        this.id = id;
        this.ReportId = ReportId;
        this.VisitId = VisitId;
        this.HeaderId = HeaderId;
        this.StoreId = StoreId;
        this.UserId = UserId;
        this.SkuId = SkuId;
        this.SkuCode = SkuCode;
        this.SkuName = SkuName;
        this.IsIr = IsIr;
        this.CustomerGroupId = CustomerGroupId;
        this.BrandId = BrandId;
        this.BrandName = BrandName;
        this.ImagePath = ImagePath;
        this.Quantity = Quantity;
        this.Depth = Depth;
        this.IsCompetitor = IsCompetitor;
        this.IsDone = IsDone;
        this.IsMedrep = IsMedrep;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    public String getReportId() {
        return ReportId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setReportId(@NotNull String ReportId) {
        this.ReportId = ReportId;
    }

    @NotNull
    public String getVisitId() {
        return VisitId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setVisitId(@NotNull String VisitId) {
        this.VisitId = VisitId;
    }

    @NotNull
    public String getHeaderId() {
        return HeaderId;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setHeaderId(@NotNull String HeaderId) {
        this.HeaderId = HeaderId;
    }

    public int getStoreId() {
        return StoreId;
    }

    public void setStoreId(int StoreId) {
        this.StoreId = StoreId;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int UserId) {
        this.UserId = UserId;
    }

    public int getSkuId() {
        return SkuId;
    }

    public void setSkuId(int SkuId) {
        this.SkuId = SkuId;
    }

    @NotNull
    public String getSkuCode() {
        return SkuCode;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setSkuCode(@NotNull String SkuCode) {
        this.SkuCode = SkuCode;
    }

    @NotNull
    public String getSkuName() {
        return SkuName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setSkuName(@NotNull String SkuName) {
        this.SkuName = SkuName;
    }

    public int getIsIr() {
        return IsIr;
    }

    public void setIsIr(int IsIr) {
        this.IsIr = IsIr;
    }

    public int getCustomerGroupId() {
        return CustomerGroupId;
    }

    public void setCustomerGroupId(int CustomerGroupId) {
        this.CustomerGroupId = CustomerGroupId;
    }

    public int getBrandId() {
        return BrandId;
    }

    public void setBrandId(int BrandId) {
        this.BrandId = BrandId;
    }

    @NotNull
    public String getBrandName() {
        return BrandName;
    }

    /** Not-null value; ensure this value is available before it is saved to the database. */
    public void setBrandName(@NotNull String BrandName) {
        this.BrandName = BrandName;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public void setImagePath(String ImagePath) {
        this.ImagePath = ImagePath;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int Quantity) {
        this.Quantity = Quantity;
    }

    public int getDepth() {
        return Depth;
    }

    public void setDepth(int Depth) {
        this.Depth = Depth;
    }

    public Integer getIsCompetitor() {
        return IsCompetitor;
    }

    public void setIsCompetitor(Integer IsCompetitor) {
        this.IsCompetitor = IsCompetitor;
    }

    public boolean getIsDone() {
        return IsDone;
    }

    public void setIsDone(boolean IsDone) {
        this.IsDone = IsDone;
    }

    public boolean getIsMedrep() {
        return IsMedrep;
    }

    public void setIsMedrep(boolean IsMedrep) {
        this.IsMedrep = IsMedrep;
    }

}
