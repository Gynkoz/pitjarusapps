package com.pitjarus.time.database;

import org.greenrobot.greendao.annotation.*;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit.

/**
 * Entity mapped to table "TSURVEY_COMPETITOR_PRODUCT".
 */
@Entity
public class TSurveyCompetitorProduct {

    @Id
    private Long id;
    private Integer UserId;
    private Integer CustomerGroupId;
    private Integer CompetitorProductId;
    private String CompetitorProductQuestion;
    private Integer CompetitorProductAnswerType;
    private String VisitId;

    @Generated
    public TSurveyCompetitorProduct() {
    }

    public TSurveyCompetitorProduct(Long id) {
        this.id = id;
    }

    @Generated
    public TSurveyCompetitorProduct(Long id, Integer UserId, Integer CustomerGroupId, Integer CompetitorProductId, String CompetitorProductQuestion, Integer CompetitorProductAnswerType, String VisitId) {
        this.id = id;
        this.UserId = UserId;
        this.CustomerGroupId = CustomerGroupId;
        this.CompetitorProductId = CompetitorProductId;
        this.CompetitorProductQuestion = CompetitorProductQuestion;
        this.CompetitorProductAnswerType = CompetitorProductAnswerType;
        this.VisitId = VisitId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getUserId() {
        return UserId;
    }

    public void setUserId(Integer UserId) {
        this.UserId = UserId;
    }

    public Integer getCustomerGroupId() {
        return CustomerGroupId;
    }

    public void setCustomerGroupId(Integer CustomerGroupId) {
        this.CustomerGroupId = CustomerGroupId;
    }

    public Integer getCompetitorProductId() {
        return CompetitorProductId;
    }

    public void setCompetitorProductId(Integer CompetitorProductId) {
        this.CompetitorProductId = CompetitorProductId;
    }

    public String getCompetitorProductQuestion() {
        return CompetitorProductQuestion;
    }

    public void setCompetitorProductQuestion(String CompetitorProductQuestion) {
        this.CompetitorProductQuestion = CompetitorProductQuestion;
    }

    public Integer getCompetitorProductAnswerType() {
        return CompetitorProductAnswerType;
    }

    public void setCompetitorProductAnswerType(Integer CompetitorProductAnswerType) {
        this.CompetitorProductAnswerType = CompetitorProductAnswerType;
    }

    public String getVisitId() {
        return VisitId;
    }

    public void setVisitId(String VisitId) {
        this.VisitId = VisitId;
    }

}
