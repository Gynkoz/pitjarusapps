package com.pitjarus.time.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import org.greenrobot.greendao.AbstractDao;
import org.greenrobot.greendao.Property;
import org.greenrobot.greendao.internal.DaoConfig;
import org.greenrobot.greendao.database.Database;
import org.greenrobot.greendao.database.DatabaseStatement;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table "TBRANDS".
*/
public class TBrandsDao extends AbstractDao<TBrands, Long> {

    public static final String TABLENAME = "TBRANDS";

    /**
     * Properties of entity TBrands.<br/>
     * Can be used for QueryBuilder and for referencing column names.
     */
    public static class Properties {
        public final static Property Id = new Property(0, Long.class, "id", true, "_id");
        public final static Property CustomerGroupId = new Property(1, Integer.class, "CustomerGroupId", false, "CUSTOMER_GROUP_ID");
        public final static Property BrandId = new Property(2, Integer.class, "BrandId", false, "BRAND_ID");
        public final static Property BrandName = new Property(3, String.class, "BrandName", false, "BRAND_NAME");
        public final static Property IsDone = new Property(4, boolean.class, "IsDone", false, "IS_DONE");
    }


    public TBrandsDao(DaoConfig config) {
        super(config);
    }
    
    public TBrandsDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(Database db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "\"TBRANDS\" (" + //
                "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                "\"CUSTOMER_GROUP_ID\" INTEGER," + // 1: CustomerGroupId
                "\"BRAND_ID\" INTEGER," + // 2: BrandId
                "\"BRAND_NAME\" TEXT," + // 3: BrandName
                "\"IS_DONE\" INTEGER NOT NULL );"); // 4: IsDone
    }

    /** Drops the underlying database table. */
    public static void dropTable(Database db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "\"TBRANDS\"";
        db.execSQL(sql);
    }

    @Override
    protected final void bindValues(DatabaseStatement stmt, TBrands entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer CustomerGroupId = entity.getCustomerGroupId();
        if (CustomerGroupId != null) {
            stmt.bindLong(2, CustomerGroupId);
        }
 
        Integer BrandId = entity.getBrandId();
        if (BrandId != null) {
            stmt.bindLong(3, BrandId);
        }
 
        String BrandName = entity.getBrandName();
        if (BrandName != null) {
            stmt.bindString(4, BrandName);
        }
        stmt.bindLong(5, entity.getIsDone() ? 1L: 0L);
    }

    @Override
    protected final void bindValues(SQLiteStatement stmt, TBrands entity) {
        stmt.clearBindings();
 
        Long id = entity.getId();
        if (id != null) {
            stmt.bindLong(1, id);
        }
 
        Integer CustomerGroupId = entity.getCustomerGroupId();
        if (CustomerGroupId != null) {
            stmt.bindLong(2, CustomerGroupId);
        }
 
        Integer BrandId = entity.getBrandId();
        if (BrandId != null) {
            stmt.bindLong(3, BrandId);
        }
 
        String BrandName = entity.getBrandName();
        if (BrandName != null) {
            stmt.bindString(4, BrandName);
        }
        stmt.bindLong(5, entity.getIsDone() ? 1L: 0L);
    }

    @Override
    public Long readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0);
    }    

    @Override
    public TBrands readEntity(Cursor cursor, int offset) {
        TBrands entity = new TBrands( //
            cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0), // id
            cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1), // CustomerGroupId
            cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2), // BrandId
            cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3), // BrandName
            cursor.getShort(offset + 4) != 0 // IsDone
        );
        return entity;
    }
     
    @Override
    public void readEntity(Cursor cursor, TBrands entity, int offset) {
        entity.setId(cursor.isNull(offset + 0) ? null : cursor.getLong(offset + 0));
        entity.setCustomerGroupId(cursor.isNull(offset + 1) ? null : cursor.getInt(offset + 1));
        entity.setBrandId(cursor.isNull(offset + 2) ? null : cursor.getInt(offset + 2));
        entity.setBrandName(cursor.isNull(offset + 3) ? null : cursor.getString(offset + 3));
        entity.setIsDone(cursor.getShort(offset + 4) != 0);
     }
    
    @Override
    protected final Long updateKeyAfterInsert(TBrands entity, long rowId) {
        entity.setId(rowId);
        return rowId;
    }
    
    @Override
    public Long getKey(TBrands entity) {
        if(entity != null) {
            return entity.getId();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasKey(TBrands entity) {
        return entity.getId() != null;
    }

    @Override
    protected final boolean isEntityUpdateable() {
        return true;
    }
    
}
