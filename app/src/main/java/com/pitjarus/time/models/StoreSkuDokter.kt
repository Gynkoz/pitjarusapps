package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class StoreSkuDokter {


    @SerializedName("dokter_id")
    var dokterId: Int = 0

    @SerializedName("dokter_code")
    var dokterCode: String = ""

    @SerializedName("dokter_name")
    var dokterName: String = ""

    @SerializedName("speciality_id")
    var specialityId: Int = 0

    @SerializedName("speciality_name")
    var specialityName: String = ""

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("territory_code")
    var territoryCode: String = ""

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("store_type_id")
    var storeTypeId: Int = 0

    @SerializedName("class_code")
    var classCode: Int = 0

    @SerializedName("class_name")
    var className: String = ""

    @SerializedName("")
    var isChecked: Int = 0

}
