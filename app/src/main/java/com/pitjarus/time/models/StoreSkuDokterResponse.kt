package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class StoreSkuDokterResponse {

    @SerializedName("items")
    var storeSkus: MutableList<StoreSkuDokter>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

}
