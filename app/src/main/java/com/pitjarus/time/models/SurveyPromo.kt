package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class SurveyPromo {
    @SerializedName("customer_group_id")
    var customerGroupId: Int? = null

    @SerializedName("promo_id")
    var promoId: Int? = null

    @SerializedName("promo_question")
    var promoQuestion: String? = null

    @SerializedName("promo_answer_type")
    var promoAnswerType: Int? = null
}
