package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class ReasonNoVisitDokter {
    @SerializedName("reason_id")
    var reasonId: Int = 0

    @SerializedName("reason_name")
    var reason: String? = null
}
