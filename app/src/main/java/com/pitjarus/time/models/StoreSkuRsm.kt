package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class StoreSkuRsm {

    @SerializedName("city_id")
    var cityId: Int = 0

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("city_name")
    var cityName: String = ""

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("channel_name")
    var channelName: String = ""

    @SerializedName("class_name")
    var className: String = ""

    @SerializedName("territory_code")
    var territoryCode: String = ""

    @SerializedName("full_name")
    var fullName: String = ""

    @SerializedName("dokter_name")
    var dokterName: String = ""

    @SerializedName("dokter_id")
    var dokterId: Int = 0

    @SerializedName("speciality_id")
    var specialityId: Int = 0

    @SerializedName("speciality_name")
    var specialityName: String = ""

    @SerializedName("")
    var isChecked: Int = 0

}
