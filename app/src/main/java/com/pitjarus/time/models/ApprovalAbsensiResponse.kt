package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class ApprovalAbsensiResponse {
    @SerializedName("approval_absensi")
    var approvalAbsensi: MutableList<ListAbsensi>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""
}
