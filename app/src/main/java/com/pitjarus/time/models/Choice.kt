package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Choice {
    @SerializedName("choice_id")
    var choiceId: Long = 0

    @SerializedName("choice_reference_id")
    var choiceReferenceId: Long = 0

    @SerializedName("choice_reference_group_id")
    var choiceReferenceGroupId: Long = 0

    @SerializedName("questioner_id")
    var questionerId: Int = 0

    @SerializedName("question_id")
    var questionId: Long = 0

    @SerializedName("choice_name")
    var choiceName: String = ""
}
