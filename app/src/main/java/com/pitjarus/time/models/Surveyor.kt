package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Surveyor {

    @SerializedName("username")
    var username: String = ""

    @SerializedName("user_id")
    var surveyorId: Int = 0

    @SerializedName("user_code")
    var surveyorCode: String = ""

    @SerializedName("full_name")
    var surveyorName: String = ""

    @SerializedName("user_type_id")
    var roleId: Int = 0

    @SerializedName("user_type_name")
    var roleName: String = ""

    @SerializedName("radius_verification_limit")
    var radiusVerificationLimit: Int = 0

    @SerializedName("city_id")
    var cityId: Int = 0

    @SerializedName("parent_id")
    var parentId: Int = 0

    @SerializedName("alarm")
    var alarm: String = ""

    @SerializedName("buka_calender")
    var bukaCalender: Int = 0

    @SerializedName("is_gps_tagging")
    var taggingGps: Int = 0

}