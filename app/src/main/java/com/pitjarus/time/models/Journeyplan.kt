package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Journeyplan {

    @SerializedName("journey_plan_id")
    var journeyPlanId: Int = 0

    @SerializedName("user_id")
    var userId: Int = 0

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("store_code")
    var storeCode: String = ""

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("call_date")
    var callDate: String = ""

    @SerializedName("address")
    var address: String = ""

    @SerializedName("latitude")
    var latitude: Double = 0.0

    @SerializedName("longitude")
    var longitude: Double = 0.0

    @SerializedName("city_id")
    var cityId: Int = 0

    @SerializedName("city_name")
    var cityName: String = ""

    @SerializedName("customer_group_id")
    var customerGroupId: Int = 0

    @SerializedName("class_id")
    var classId: Int = 0

    @SerializedName("is_active")
    var isActive: Int = 0

    @SerializedName("last_visited_utc")
    var lastVisitedUtc: String = ""

    @SerializedName("last_visited")
    var lastVisited: String = ""

    @SerializedName("photo_path")
    var photoPath: String = ""

    @SerializedName("branch_id")
    var branchId: Int = 0

    @SerializedName("branch_name")
    var branchName: String = ""

    @SerializedName("region_id")
    var regionId: Int = 0

    @SerializedName("region_name")
    var regionName: String = ""

    @SerializedName("customer_group_name")
    var customerGroupName: String = ""

    @SerializedName("channel_id")
    var channelId: Int = 0

    @SerializedName("display_column")
    var displayColumn: Int = 0

    @SerializedName("display_row")
    var displayRow: Int = 0

    @SerializedName("store_class_id")
    var storeClassId: Int = 0

    @SerializedName("channel_name")
    var channelName: String = ""

    @SerializedName("class_name")
    var className: String = ""

    @SerializedName("attribut_name")
    var attributName: String = ""

    @SerializedName("territory_name")
    var territoryName: String = ""

    @SerializedName("territory_code")
    var territoryCode: String = ""

    @SerializedName("is_reporting")
    var isReporting: Int = 0

    @SerializedName("is_approved")
    var isApproved: Int = 0


}
