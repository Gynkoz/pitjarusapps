package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class StoreSkuResponse {

    @SerializedName("items")
    var storeSkus: MutableList<StoreSku>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

}
