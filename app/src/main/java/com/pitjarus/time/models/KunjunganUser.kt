package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class KunjunganUser {

    @SerializedName("actual_visit")
    var actualVisit: Int = 0

    @SerializedName("garuda_store")
    var garudaStore: Int = 0

    @SerializedName("garuda_score")
    var garudaScore: Int = 0

    @SerializedName("target")
    var target: Int = 0

    @SerializedName("actual")
    var actual: Int = 0

    @SerializedName("percentage")
    var percentage: Int = 0

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

}
