package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class JourneyplanInsert {

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

}
