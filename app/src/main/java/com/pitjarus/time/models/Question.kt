package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Question {
    @SerializedName("question_id")
    var questionId: Long = 0

    @SerializedName("questioner_id")
    var questionerId: Int = 0

    @SerializedName("question_name")
    var questionName: String = ""

    @SerializedName("question_type")
    var questionType: String = ""

    @SerializedName("question_number")
    var questionNumber: Int = 0

    @SerializedName("is_required")
    var isRequired: Int = 0

    @SerializedName("is_cascaded")
    var isCascaded: Int = 0

    @SerializedName("has_child")
    var hasChild: Int = 0

    @SerializedName("child_count")
    var childCount: Int = 0

    @SerializedName("parent_question_is_required")
    var parentQuestionIsRequired: Int = 0

    @SerializedName("parent_question_is_required_number")
    var parentQuestionIsRequiredNumber: Int = 0

    @SerializedName("parent_question_is_required_answer")
    var parentQuestionIsRequiredAnswer: String = ""

    @SerializedName("choices")
    var choices: List<Choice>? = null
}

