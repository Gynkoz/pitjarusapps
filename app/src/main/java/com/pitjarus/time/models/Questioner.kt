package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Questioner {
    @SerializedName("questioner_id")
    var questionerId: Int = 0

    @SerializedName("questioner_name")
    var questionerName: String = ""

    @SerializedName("subtitle")
    var subTitle: String = ""

    @SerializedName("start_date")
    var startDate: String = ""

    @SerializedName("end_date")
    var endDate: String = ""

    @SerializedName("sorting_number")
    var sortingNumber: Int = 0

    @SerializedName("questions")
    var questions: List<Question>? = null
}
