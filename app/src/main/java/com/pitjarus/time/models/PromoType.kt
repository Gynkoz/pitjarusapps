package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class PromoType {
    @SerializedName("promo_type_id")
    var promoTypeId: Int = 0

    @SerializedName("promo_type_name")
    var promoTypeName: String? = null
}
