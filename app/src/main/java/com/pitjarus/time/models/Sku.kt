package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Sku {
    @SerializedName("sku_id")
    var skuId: Int? = null

    @SerializedName("sku_code")
    var skuCode: String? = null

    @SerializedName("sku_name")
    var skuName: String? = null

    @SerializedName("customer_group_id")
    var customerGroupId: Int? = null

    @SerializedName("product_group_name")
    var productGroupName: String? = null

    @SerializedName("brand_id")
    var brandId: Int? = null

    @SerializedName("brand_name")
    var brandName: String? = null

    @SerializedName("is_ir")
    var isIr: Int? = null

    @SerializedName("is_competitor")
    var isCompetitor: Int? = null
}
