package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName
import com.pitjarus.time.models.*

class LoginResponse {
    @SerializedName("surveyor")
    var surveyor: Surveyor? = null

    @SerializedName("stores")
    var stores: List<Store>? = null

    @SerializedName("distributor")
    var distributor: List<Distributor>? = null

    @SerializedName("brands")
    var brands: List<Brands>? = null

    @SerializedName("promo_type")
    var promoType: List<PromoType>? = null

    @SerializedName("competitor_price")
    var competitorPrice: List<CompetitorPrice>? = null

    @SerializedName("competitor_product")
    var competitorProduct: List<CompetitorProduct>? = null

    @SerializedName("dokter")
    var dokter: List<Dokter>? = null

    @SerializedName("sku_by_customer_group")
    var sku: List<Sku>? = null

//    @SerializedName("approval_store")
//    var approvalStore: List<ApprovalStore>? = null

    @SerializedName("reason_no_visit")
    var reasonNoVisits: List<ReasonNoVisit>? = null

    @SerializedName("reason_no_visit_dokter")
    var reasonNoVisitDokter: List<ReasonNoVisitDokter>? = null

    @SerializedName("reason_oos")
    var reasonOos: List<ReasonOos>? = null

    @SerializedName("bobot_harian_type")
    var reasonTBobotHarianType: List<ReasonBobot>? = null

    @SerializedName("activity_type")
    var activityType: List<ActivityType>? = null

    @SerializedName("survey_promo")
    var surveyPromo: List<SurveyPromo>? = null

    @SerializedName("survey_competitor_price")
    var surveyCompetitorPrice: List<SurveyCompetitorPrice>? = null

    @SerializedName("survey_competitor_product")
    var surveyCompetitorProduct: List<SurveyCompetitorProduct>? = null


    @SerializedName("questioners")
    var questioners: List<Questioner>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""
}
