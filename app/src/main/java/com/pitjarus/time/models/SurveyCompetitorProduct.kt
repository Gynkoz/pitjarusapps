package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class SurveyCompetitorProduct {
    @SerializedName("customer_group_id")
    var customerGroupId: Int? = null

    @SerializedName("competitor_product_id")
    var competitorProductId: Int? = null

    @SerializedName("competitor_product_question")
    var competitorProductQuestion: String? = null

    @SerializedName("competitor_product_answer_type")
    var competitorProductAnswerType: Int? = null
}
