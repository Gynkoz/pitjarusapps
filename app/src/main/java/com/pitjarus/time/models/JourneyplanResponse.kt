package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class JourneyplanResponse {

    @SerializedName("items")
    var journeyplans: MutableList<Journeyplan>? = null

    @SerializedName("itemSku")
    var sku: MutableList<Sku>? = null

    @SerializedName("itemBrand")
    var brands: MutableList<Brands>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

}
