package com.pitjarus.time.models

import com.pitjarus.time.database.TStore

class StoreViewModel {
    var store: TStore? = null
    var distance: Float = 0.toFloat()
}
