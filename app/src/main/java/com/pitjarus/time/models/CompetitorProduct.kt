package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class CompetitorProduct {
    @SerializedName("competitor_new_product_type_id")
    var competitorNewProductTypeId: Int = 0

    @SerializedName("competitor_new_product_type_name")
    var competitorNewProductTypeName: String? = null
}
