package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Brands {
    @SerializedName("customer_group_id")
    var customerGroupId: Int = 0

    @SerializedName("brand_id")
    var brandId: Int = 0

    @SerializedName("brand_name")
    var brandName: String? = null
}
