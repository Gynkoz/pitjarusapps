package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Weather {
    @SerializedName("id")
    var id: Int = 0

    @SerializedName("main")
    var info: String=""
}
