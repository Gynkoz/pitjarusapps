package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class JourneyplanDokterResponse {

    @SerializedName("items")
    var journeyplansDokter: MutableList<JourneyplanDokter>? = null

    @SerializedName("itemSku")
    var sku: MutableList<Sku>? = null

    @SerializedName("itemBrand")
    var brands: MutableList<Brands>? = null

    @SerializedName("itemDokter")
    var dokter: MutableList<Dokter>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

}
