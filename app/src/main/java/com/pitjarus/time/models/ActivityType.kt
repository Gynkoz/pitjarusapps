package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class ActivityType {
    @SerializedName("activity_type_id")
    var activityTypeId: Int = 0

    @SerializedName("activity_type_name")
    var activityTypeName: String? = null
}
