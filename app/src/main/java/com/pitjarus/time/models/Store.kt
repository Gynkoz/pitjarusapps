package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Store {

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("store_code")
    var storeCode: String = ""

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("address")
    var address: String = ""

    @SerializedName("city_id")
    var cityId: Int = 0

    @SerializedName("city_name")
    var cityName: String = ""

    @SerializedName("branch_id")
    var branchId: Int = 0

    @SerializedName("branch_name")
    var branchName: String = ""

    @SerializedName("region_id")
    var regionId: Int = 0

    @SerializedName("region_name")
    var regionName: String = ""

    @SerializedName("customer_group_id")
    var customerGroupId: Int = 0

    @SerializedName("customer_group_name")
    var customerGroupName: String = ""

    @SerializedName("channel_id")
    var channelId: Int = 0

    @SerializedName("channel_name")
    var channelName: String = ""

    @SerializedName("store_class_id")
    var storeClassId: Int = 0

    @SerializedName("class_name")
    var storeClassName: String = ""

    @SerializedName("territory_code")
    var territoryCode: String = ""

    @SerializedName("territory_name")
    var territoryName: String = ""

    @SerializedName("latitude")
    var latitude: Double = 0.toDouble()

    @SerializedName("longitude")
    var longitude: Double = 0.toDouble()

    @SerializedName("last_visited")
    var lastVisited: String = ""

    @SerializedName("display_column")
    var displayColumn: Int = 0

    @SerializedName("display_row")
    var displayRow: Int = 0

    @SerializedName("photo_path")
    var photoPath: String = ""

    @SerializedName("is_reporting")
    var isReporting: Int = 0

    @SerializedName("visit")
    var isStoreVisit: Int = 0

    @SerializedName("dokter_id")
    var dokterId: Int = 0

    @SerializedName("dokter_name")
    var dokterName: String = ""

    @SerializedName("dokter_code")
    var dokterCode: String = ""

    @SerializedName("speciality_name")
    var specialityName: String = ""

    @SerializedName("sub_speciality_name")
    var subSpecialityName: String = ""

    @SerializedName("signature1")
    var signature1: String = ""

    @SerializedName("signature1_date")
    var signature1date: String = ""

    @SerializedName("signature2")
    var signature2: String = ""

    @SerializedName("signature2_date")
    var signature2date: String = ""

    @SerializedName("")
    var isChecked: Int = 0

}
