package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class StoreSku {

    @SerializedName("city_id")
    var cityId: Int = 0

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("city_name")
    var cityName: String = ""

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("channel_name")
    var channelName: String = ""

    @SerializedName("class_name")
    var className: String = ""

    @SerializedName("territory_code")
    var territoryCode: String = ""

    @SerializedName("full_name")
    var fullName: String = ""

    @SerializedName("visit")
    var isStoreVisit: Int = 0

    @SerializedName("")
    var isChecked: Int = 0

}
