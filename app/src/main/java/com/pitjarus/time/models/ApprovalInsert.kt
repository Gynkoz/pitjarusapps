package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class ApprovalInsert {

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

}
