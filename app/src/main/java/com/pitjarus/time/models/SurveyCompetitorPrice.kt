package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class SurveyCompetitorPrice {
    @SerializedName("customer_group_id")
    var customerGroupId: Int? = null

    @SerializedName("competitor_price_id")
    var competitorPriceId: Int? = null

    @SerializedName("competitor_price_question")
    var competitorPriceQuestion: String? = null

    @SerializedName("competitor_price_answer_type")
    var competitorPriceAnswerType: Int? = null
}
