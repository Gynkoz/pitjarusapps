package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Product {

    @SerializedName("product_id")
    var productId: Int = 0

    @SerializedName("product_code")
    var productCode: String = ""

    @SerializedName("product_name")
    var productName: String = ""

    @SerializedName("tipesub_id")
    var typesubId: Int = 0

    @SerializedName("unit_id")
    var unitId: Int = 0

    @SerializedName("unit_name")
    var unitName: String = ""

    @SerializedName("shape_id")
    var shapeId: Int = 0

    @SerializedName("shape_name")
    var shapeName: String = ""

    @SerializedName("tipe_id")
    var typeId: Int = 0

    @SerializedName("tipe_name")
    var typeName: String = ""

    @SerializedName("brand_id")
    var brandId: Int = 0

    @SerializedName("brand_Name")
    var brandName: String = ""

    @SerializedName("image_path")
    var photoPath: String = ""

    @SerializedName("is_competitor")
    var isCompetitor: Int = 0

    @SerializedName("standard_oos")
    var standardOos: Int = 0


}
