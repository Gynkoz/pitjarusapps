package com.pitjarus.time.models

import android.content.Context
import android.util.Log
import com.pitjarus.time.utils.Config
import com.pitjarus.time.utils.SharedPrefsUtils

object Login {
    fun logout(context: Context) {
        var username=Login.getUsername(context)
        var saved=Login.getIsUsernameSaved(context)

        Log.i("username",username)
        Log.i("usernamesaved",saved.toString())

        SharedPrefsUtils.clearPreferences(context)
        if(saved){
            SharedPrefsUtils.setStringPreference(context, Config.KEY_EMPLOYEE_USERNAME, username!!)
            SharedPrefsUtils.setBooleanPreference(context, Config.KEY_EMPLOYEE_USERNAME_SAVED, true)
        }



    }

    fun clearSharedPrefVisit(context: Context) {
        SharedPrefsUtils.setStringPreference(context, Config.KEY_VISIT_ID, "")
        SharedPrefsUtils.setIntegerPreference(context, Config.KEY_STORE_ID, 0)
        SharedPrefsUtils.setIntegerPreference(context, Config.KEY_STORE_CHANNEL, 0)
        SharedPrefsUtils.setStringPreference(context, Config.KEY_STORE_CODE, "")
        SharedPrefsUtils.setStringPreference(context, Config.KEY_STORE_NAME, "")
        SharedPrefsUtils.setIntegerPreference(context, Config.KEY_CHAINSTORE_ID, 0)
        SharedPrefsUtils.setStringPreference(context, Config.KEY_IN_REPORT, "")
        SharedPrefsUtils.setStringPreference(context, Config.KEY_SIGNATURE_PATH, "")
        SharedPrefsUtils.setIntegerPreference(context, Config.KEY_STORE_CUSTOMER_GROUP_ID, 0)
        SharedPrefsUtils.setStringPreference(context, Config.KEY_NOTE, "")

        SharedPrefsUtils.setBooleanPreference(context, Config.KEY_CHECKED_REPORT_INVESTMENT, false)
    }

    fun isLoggedIn(context: Context): Boolean =
            SharedPrefsUtils.getBooleanPreference(context, Config.KEY_IS_LOGGED_IN, false)

    fun getUserId(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_EMPLOYEE_ID, 0)

    fun getCityId(context: Context): Int =
        SharedPrefsUtils.getIntegerPreference(context, Config.KEY_CITY_ID, 0)

    fun getTeritoryCode(context: Context): String? =
        SharedPrefsUtils.getStringPreference(context, Config.KEY_TERITORY_CODE, "NULL")

    fun getUsername(context: Context): String? =
        SharedPrefsUtils.getStringPreference(context, Config.KEY_EMPLOYEE_USERNAME, "NULL")

    fun getEmployeeName(context: Context): String? =
        SharedPrefsUtils.getStringPreference(context, Config.KEY_EMPLOYEE_NAME, "NULL")

    fun getVisitId(context: Context): String? =
        SharedPrefsUtils.getStringPreference(context, Config.KEY_VISIT_ID, "NULL")

    fun getStoreId(context: Context): Int =
        SharedPrefsUtils.getIntegerPreference(context, Config.KEY_STORE_ID, 0)

    fun getParentId(context: Context): Int =
        SharedPrefsUtils.getIntegerPreference(context, Config.KEY_PARENT_ID, 0)

    fun getSignature(context: Context): String? =
        SharedPrefsUtils.getStringPreference(context, Config.KEY_SIGNATURE_PATH, "NULL")

    fun getSignatureNote(context: Context): String? =
        SharedPrefsUtils.getStringPreference(context, Config.KEY_NOTE, "NULL")

    fun getEmployeeRoleName(context: Context): String? =
            SharedPrefsUtils.getStringPreference(context, Config.KEY_EMPLOYEE_ROLE_NAME, "NULL")

    fun getEmployeeRoleId(context: Context): Int =
            SharedPrefsUtils.getIntegerPreference(context, Config.KEY_EMPLOYEE_ROLE_ID, 0)

    fun getWeekNow(context: Context): Int =
        SharedPrefsUtils.getIntegerPreference(context, Config.KEY_MINGGU_KE, 0)

    fun isAbsenIn(context: Context): Boolean =
        SharedPrefsUtils.getIntegerPreference(context, Config.KEY_IS_ABSENT, 0)==1

    fun getIsUsernameSaved(context: Context): Boolean =
        SharedPrefsUtils.getBooleanPreference(context, Config.KEY_EMPLOYEE_USERNAME_SAVED, false)

    fun isCalenderOpen(context: Context): Int =
        SharedPrefsUtils.getIntegerPreference(context, Config.KEY_BUKA_CALENDER, 0)
}