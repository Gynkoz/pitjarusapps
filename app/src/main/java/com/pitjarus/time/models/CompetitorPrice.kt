package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class CompetitorPrice {
    @SerializedName("competitor_price_type_id")
    var competitorPriceTypeId: Int = 0

    @SerializedName("competitor_price_type_name")
    var competitorPriceTypeName: String? = null
}
