package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class ApprovalStoreResponse {
    @SerializedName("approval_store")
    var approvalStore: MutableList<ApprovalStore>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""
}
