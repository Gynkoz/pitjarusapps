package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class ListAbsensi {
    @SerializedName("user_id")
    var userId: Int? = null

    @SerializedName("user_code")
    var userCode: String? = null

    @SerializedName("username")
    var username: String? = null

    @SerializedName("full_name")
    var fullName: String? = null

    @SerializedName("user_type_id")
    var userTypeId: Int? = null

    @SerializedName("parent_id")
    var parentId: Int? = null

    @SerializedName("activity_type_id")
    var activityTypeId: Int? = null

    @SerializedName("activity_type_name")
    var activityTypeName: String? = null

    @SerializedName("start_date")
    var startDate: String? = null

    @SerializedName("end_date")
    var endDate: String? = null

    @SerializedName("remarks")
    var remarks: String? = null

    @SerializedName("photo_path")
    var photoPath: String? = null

    @SerializedName("report_absensi_id")
    var reportAbsensiId: Int? = null
}