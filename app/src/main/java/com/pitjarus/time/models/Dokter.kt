package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Dokter {
    @SerializedName("dokter_id")
    var dokterId: Int? = null

    @SerializedName("store_id")
    var storeId: Int? = null

    @SerializedName("dokter_code")
    var dokterCode: String? = null

    @SerializedName("dokter_name")
    var dokterName: String? = null

    @SerializedName("class_name")
    var className: String? = null

    @SerializedName("speciality_name")
    var specialityName: String? = null

    @SerializedName("sub_speciality_name")
    var subSpecialityName: String? = null

    @SerializedName("signature1")
    var signature1: String = ""

    @SerializedName("signature2")
    var signature2: String = ""

    @SerializedName("signature1_date")
    var signature1Date: String = ""

    @SerializedName("signature2_date")
    var signature2Date: String = ""

    @SerializedName("visit")
    var isDokterVisit: Int = 0


}
