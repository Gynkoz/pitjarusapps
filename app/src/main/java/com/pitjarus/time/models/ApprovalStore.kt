package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class ApprovalStore {
    @SerializedName("user_id")
    var userId: Int = 0

    @SerializedName("username")
    var username: String = ""

    @SerializedName("full_name")
    var fullName: String = ""

    @SerializedName("dokter_name")
    var dokterName: String = ""

    @SerializedName("is_approved")
    var isApproved: Int = 0

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("store_name")
    var storeName: String = ""

    @SerializedName("address")
    var address: String = ""

    @SerializedName("call_date")
    var date:  String = ""

    @SerializedName("spesialis_name")
    var spesialisName:  String = ""

    @SerializedName("class_name")
    var className:  String = ""

    @SerializedName("journey_plan_id")
    var journeyPlanId:  Int = 0
}