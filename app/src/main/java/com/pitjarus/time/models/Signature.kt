package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class Signature {
    @SerializedName("signature_path")
    var signaturePath: String = ""

    @SerializedName("store_id")
    var storeId: Int = 0

    @SerializedName("dokter_id")
    var dokterId: Int = 0
}
