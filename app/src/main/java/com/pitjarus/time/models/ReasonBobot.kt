package com.pitjarus.time.models

import com.google.gson.annotations.SerializedName

class ReasonBobot {
    @SerializedName("id_bobot")
    var bobotId: Int = 0

    @SerializedName("nama_bobot")
    var bobotName: String? = null
}
