package com.pitjarus.time.models

import android.graphics.RectF

class Recognition(
    val id: String?,
    val title: String?,
    val confidence: Float?,
    val location: RectF?
) {

    override fun toString(): String {
        var resultString = ""
        if (id != null) {
            resultString += "[$id] "
        }
        if (title != null) {
            resultString += "$title "
        }
        if (confidence != null) {
            resultString += String.format("(%.1f%%) ", confidence * 100.0f)
        }
        if (location != null) {
            resultString += "$location "
        }
        return resultString.trim { it <= ' ' }
    }

}