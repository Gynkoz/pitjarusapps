package com.pitjarus.time.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.support.v4.app.NotificationCompat
import android.widget.Toast
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.pitjarus.time.R
import com.pitjarus.time.activities.LoginActivity
import com.pitjarus.time.activities.StoreListActivity
import com.pitjarus.time.models.Login
import com.pitjarus.time.utils.Config
import com.pitjarus.time.utils.SharedPrefsUtils

class NotifyWorker(_context: Context, params: WorkerParameters) : Worker(_context, params) {
    val title = "Time App."
    val text = "Anda belum logout aplikasi Time hari ini, silahkan logout."
    private val context = _context

    override fun doWork(): Result {
        if(Login.isLoggedIn(context)) {
            notify(context)
        }
        return Result.SUCCESS
    }

    private fun notify(context: Context) {
        try {
        val intent = Intent(context, LoginActivity::class.java)
//        intent.putExtra("FromNotification", true)
//        intent.putExtra("Message", text)
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                val nManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val channelId = "ChannelId101"
                val channelName = "ChannelName101"
                val importance = NotificationManager.IMPORTANCE_HIGH
                val nChannel = NotificationChannel(channelId, channelName, importance)
                nChannel.enableLights(true)
                nChannel.lightColor = Color.BLUE
                nChannel.enableVibration(true)
                nChannel.vibrationPattern = longArrayOf(250, 250, 250, 250)
                nManager.createNotificationChannel(nChannel)

                val mBuilder = NotificationCompat.Builder(context, channelId)
                        .setSmallIcon(R.drawable.ic_logo_taisho)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setContentIntent(pendingIntent)
                        .setStyle(NotificationCompat.BigTextStyle().bigText(text))
                        .setAutoCancel(true)
                nManager.notify(1, mBuilder.build())
                SharedPrefsUtils.setBooleanPreference(context, Config.KEY_ALARM, true)

            } else {
                val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                val mBuilder = NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_logo_taisho)
                        .setContentTitle(title)
                        .setContentText(text)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setVibrate(longArrayOf(250, 250, 250, 250))
                        .setLights(Color.BLUE, 1000, 1000)
                        .setStyle(NotificationCompat.BigTextStyle().bigText(text))
                        .setAutoCancel(true)
                val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.notify(1, mBuilder.build())
                SharedPrefsUtils.setBooleanPreference(context, Config.KEY_ALARM, true)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showToast(context: Context) {
        Handler(Looper.getMainLooper()).post {
            Toast.makeText(context, text, Toast.LENGTH_LONG).show()
        }
    }
}