package com.pitjarus.time.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.pitjarus.time.R
import com.pitjarus.time.models.StoreViewModel
import com.pitjarus.time.utils.Config
import com.pitjarus.time.utils.Helper

class StoresAdapter(private val models: List<StoreViewModel>, private val rowLayout: Int, private val radiusVerificationLimit: Int, private val onItemClickListener: OnItemClickListener)
    : RecyclerView.Adapter<StoresAdapter.ViewHolder>() {

    interface OnItemClickListener {
        fun onItemClick(item: StoreViewModel)
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {
        var description: TextView = v.findViewById(R.id.row_lblDescription)
        var storeName: TextView = v.findViewById(R.id.row_lblStoreName)
        var distance: TextView = v.findViewById(R.id.row_lblDistance)
        var imgChecked: ImageView = v.findViewById(R.id.row_imgCheck)
        var imgMarker: ImageView = v.findViewById(R.id.row_imgMarker)
        var address: TextView = v.findViewById(R.id.row_lblAddress)

        fun bind(item: StoreViewModel, onItemClickListener: OnItemClickListener) {
            itemView.setOnClickListener { onItemClickListener.onItemClick(item) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(rowLayout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = models[position]
        val store = model.store!!

            holder.storeName.text = store.storeName
            holder.description.text = store.storeCode
            holder.address.text = "${store.address}, ${store.storeClassName} / ${store.territoryName}"
            holder.distance.text = Helper.convertMeterToKm(model.distance)

        if (model.store!!.latitude == 0.toDouble() && model.store!!.longitude == 0.toDouble()) {
            holder.imgMarker.setImageResource(R.drawable.ic_marker_grey)
            holder.distance.text = "N/A"
        } else {
            when {
                model.distance <= radiusVerificationLimit - 20 -> holder.imgMarker.setImageResource(R.drawable.ic_marker_green)
                model.distance <= radiusVerificationLimit -> holder.imgMarker.setImageResource(R.drawable.ic_marker_yellow)
                else -> holder.imgMarker.setImageResource(R.drawable.ic_marker_red)
            }
        }

        if (store.isVisited == Config.YES_CODE || store.isStoreVisit==1 || store.isStoreVisit==0) {
            holder.imgChecked.setImageResource(R.drawable.ic_check_circle_24dp)
        } else {
            holder.imgChecked.setImageResource(R.drawable.ic_check_circle_gray_24dp)
        }

        holder.bind(models[position], onItemClickListener)
    }

    override fun getItemCount(): Int {
        return models.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}
