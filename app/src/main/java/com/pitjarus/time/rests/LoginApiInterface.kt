package com.pitjarus.time.rests

import com.pitjarus.time.models.*
import com.pitjarus.time.rests.responses.GenericListResponse
import retrofit2.Call
import retrofit2.http.*

interface LoginApiInterface {
    @FormUrlEncoded
    @POST("login/login_pitjarus_apps")
    fun getLoginData(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("android_id") androidId: String,
        @Field("app_version") appVersion: String,
        @Field("device_data") deviceData: String,
        @Field("datetime_utc") datetime: Long,
        @Field("ip_address") ipAddress: String,
        @Field("android_version") androidVersion: Int,
        @Field("reg_id") reg_id: String
        ): Call<LoginResponse>

    @GET("login/getDashboardData")
    fun getKunjunganUser(
        @Query("surveyor_id") surveyorId: Int,
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String): Call<KunjunganUser>

    @GET("login/getDashboardDataDokter")
    fun getKunjunganUserDokter(
        @Query("surveyor_id") surveyorId: Int,
        @Query("start_date") startDate: String,
        @Query("end_date") endDate: String): Call<KunjunganUser>

    @GET("weather")
    fun getWeather(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") appId: String,
        @Query("units") units: String): Call<WeatherResponse>

    @GET("forecast")
    fun getWeatherNext(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("appid") appId: String,
        @Query("units") units: String): Call<ListWeather>

}