package com.pitjarus.time.rests.responses

import com.google.gson.annotations.SerializedName

class GenericListResponse<T> {
    @SerializedName("items")
    var items: MutableList<T>? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""
}
