package com.pitjarus.time.rests.responses

import com.google.gson.annotations.SerializedName

class GenericResponse<T> {
    @SerializedName("item")
    var item: T? = null

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""
}
