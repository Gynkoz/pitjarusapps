package com.pitjarus.time.rests

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {
    private const val BASE_URL = "https://taisho-time-api.pitjarus.co/"
    private val BASE_URL2 = "https://api.openweathermap.org/data/2.5/"


    private var retrofit: Retrofit? = null
    private var retrofit2: Retrofit? = null


    val client: Retrofit?
        get() {
//            if (retrofit == null) {
                val okHttpClient = OkHttpClient.Builder()
                        .readTimeout(120, TimeUnit.SECONDS)
                        .connectTimeout(120, TimeUnit.SECONDS)
                        .build()
                retrofit = Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .client(okHttpClient)
                        .build()
//            }

            return retrofit
        }

    val client2: Retrofit?
        get() {
//            if (retrofit == null) {
                val okHttpClient = OkHttpClient.Builder()
                    .readTimeout(120, TimeUnit.SECONDS)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .build()
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL2)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build()
//            }

            return retrofit
        }



    val hostUrl: String
        get() = "https://taisho-time-api.pitjarus.co/"

    val hostUrlUpdate: String
        get() = "https://taisho-time-api.pitjarus.co/"

    fun getUpdateStoreLocationUrl(): String {
        return BASE_URL + "generalReport/updateStoreLocation"
    }

    fun getUpdateComplaintUrl(): String {
        return BASE_URL + "response/Complaint/updatecomplaint" //beda dengan response
    }

    fun getInsertVisitUrl(): String {
        return BASE_URL + "generalReport/checkInVisit1"
    }

    fun getInsertVisitDokter(): String {
        return BASE_URL + "generalReport/insertVisitDokter"
    }

    fun getInsertPhotoDetailChiller(): String {
        return BASE_URL + "generalReport/insertChillerDisplayDetailPhoto"
    }

    fun getInsertApproval(): String {
        return BASE_URL + "generalReport/approvalReport3"
    }

    fun getInsertApprovalStore(): String {
        return BASE_URL + "generalReport/approvalStoreReport"
    }

    fun getInsertApprovalStoreDokter(): String {
        return BASE_URL + "generalReport/approvalStoreDokterReport"
    }

    fun getInsertPhotoSurvey(): String {
        return BASE_URL + "generalReport/insertAdditionalPhoto"
    }

    fun getInsertPhotoDetailReguler(): String {
        return BASE_URL + "generalReport/insertRegulerDisplayDetailPhoto"
    }

    fun getInsertPosm(): String {
        return BASE_URL + "generalReport/insertPosm"
    }

    fun getInsertPhotoCompetitor(): String {
        return BASE_URL + "generalReport/CompetitorPhotoReport"
    }

    fun getInsertAbsensiUrl(): String {
        return BASE_URL + "generalReport/insertAbsensi"
    }
    fun getInsertLokasiUrl(): String {
        return BASE_URL + "generalReport/insertLokasiLogin"
    }

    fun getInsertKunjunganDokter(): String {
        return BASE_URL + "generalReport/insertKunjunganDokter"
    }

    fun getInsertPhotoVisit(): String {
        return BASE_URL + "generalReport/ReportPhotoVisit"
    }
    fun getFreeDisplayPhotoReport(): String {
        return BASE_URL + "generalReport/FreeDisplayPhotoReport"
    }
    fun getInvestmentPhotoReport(): String {
        return BASE_URL + "generalReport/InvestmentPhotoReport"
    }

    fun getInsertGeneralReport(): String {
        return BASE_URL + "generalReport/generalReport"
    }

    fun getInsertIrReport(): String {
        return BASE_URL + "generalReport/irReport"
    }

    fun getInsertAdditionalReport(): String {
        return BASE_URL + "generalReport/additionalReport"
    }

    fun getUpdateVisitOutUrl(): String {
        return BASE_URL + "generalReport/checkOutVisit"
    }

    fun getUpdateVisitOutDokterUrl(): String {
        return BASE_URL + "generalReport/checkOutVisitDokter"
    }

    fun getResetStore(): String {
        return BASE_URL + "generalReport/sendResetLokasi"
    }

    fun getInsertQuestionerlUrl(): String {
        return BASE_URL + "generalReport/insertQuestioner"
    }

}
