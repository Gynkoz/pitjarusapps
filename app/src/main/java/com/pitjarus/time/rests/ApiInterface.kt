package com.pitjarus.time.rests

//import com.pitjarus.time.models.ComplaintListResponse
//import com.pitjarus.time.models.ComplaintResponse
import com.pitjarus.time.models.*
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

//    @FormUrlEncoded
//    @POST("ReportGaruda/deleteChiller")
//    fun deleteChiller(
//            @Field("pemasangan_id") modifiedDate: String): Call<ReportResponse>

    @GET("login/deleteJourneyplan")
    fun deleteJourneyplan(
        @Query("journeyplan_id") journeyplanId: Int): Call<ReportResponse>

    @GET("login/deleteJourneyplanDokter")
    fun deleteJourneyplanDokter(
        @Query("journeyplan_dokter_id") journeyplanDokterId: Int): Call<ReportResponse>




    @FormUrlEncoded
    @POST("generalReport/reportJourneyPlan")
    fun getInsertJourneyplan(
        @Field("user_id") userId: Int,
        @Field("store_list") storeList: String,
        @Field("date_time") dateTime: String
    ): Call<JourneyplanInsert>

    @FormUrlEncoded
    @POST("generalReport/reportJourneyPlanRsm")
    fun getInsertJourneyplanRsm(
        @Field("user_id") userId: Int,
        @Field("store_list") storeList: String,
        @Field("date_time") dateTime: String
    ): Call<JourneyplanInsert>

    @FormUrlEncoded
    @POST("generalReport/reportJourneyPlanKae")
    fun getInsertJourneyplanKae(
        @Field("user_id") userId: Int,
        @Field("store_list") storeList: String,
        @Field("date_time") dateTime: String
    ): Call<JourneyplanInsert>

    @FormUrlEncoded
    @POST("generalReport/reportJourneyPlanDokter")
    fun getInsertJourneyplanDokter(
        @Field("user_id") userId: Int,
        @Field("store_list") storeList: String,
        @Field("date_time") dateTime: String
    ): Call<JourneyplanInsert>

    @FormUrlEncoded
    @POST("generalReport/reportJourneyPlanDokterHis")
    fun getInsertJourneyplanDokterHis(
        @Field("user_id") userId: Int,
        @Field("store_list") storeList: String,
        @Field("date_time") dateTime: String
    ): Call<JourneyplanInsert>

    @FormUrlEncoded
    @POST("generalReport/reportJourneyPlanAtasan")
    fun getInsertJourneyplanAtasan(
        @Field("user_id") userId: Int,
        @Field("store_list") storeList: String,
        @Field("date_time") dateTime: String,
        @Field("user_type_id") userTypeId: Int
    ): Call<JourneyplanInsert>



    @GET("login/getApprovalStore")
    fun getApprovalStore(
        @Query("user_id") userId: Int): Call<ApprovalStoreResponse>

    @GET("login/getApprovalAbsensi")
    fun getApprovalAbsensi(
        @Query("user_id") userId: Int): Call<ApprovalAbsensiResponse>


    @FormUrlEncoded
    @POST("generalReport/approvalReportStore")
    fun getInsertApprovalStore(
        @Field("report_id") reportId: String,
        @Field("user_id") userId: Int
    ): Call<ApprovalInsert>

    @FormUrlEncoded
    @POST("generalReport/approvalReportStoreDokter")
    fun getInsertApprovalStoreDokter(
        @Field("report_id") reportId: String,
        @Field("user_id") userId: Int
    ): Call<ApprovalInsert>



    @FormUrlEncoded
    @POST("generalReport/approvalAbsensiReport")
    fun getInsertApprovalAbsensi(
        @Field("report_id") reportId: String,
        @Field("user_id") userId: Int

    ): Call<ApprovalInsert>




//    @GET("response/Complaint/getComplaint")
//    fun getComplaint(
//            @Query("complaint_id") complaintId: Int): Call<ComplaintResponse>
//
//    @GET("response/Complaint/getComplaintsByStoreId")
//    fun getComplaintsByStore(
//            @Query("store_id") storeId: Int): Call<ComplaintListResponse>
//
//    @GET("response/Complaint/getCounterBatch")
//    fun getCounterBatch(
//            @Query("store_id") storeId: Int): Call<ComplaintListResponse>

}