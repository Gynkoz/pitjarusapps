package com.pitjarus.time.serviceFCM

/**
 * Created by Pitjarus on 12/10/2017.
 */

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import android.support.v4.app.NotificationCompat
import android.support.v4.content.LocalBroadcastManager
import com.pitjarus.time.R
import com.pitjarus.time.activities.LoginActivity
import com.pitjarus.time.activities.MainMenuActivity
import com.pitjarus.time.models.Login
import com.pitjarus.time.utils.Config
import com.pitjarus.time.utils.NotificationUtils
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import org.json.JSONException
import org.json.JSONObject

import java.util.Date


/**
 * Created by Ravi Tamada on 08/08/16.
 * www.androidhive.info
 */
class MyFirebaseMessagingService : FirebaseMessagingService() {

    private var notificationUtils: NotificationUtils? = null

    override fun onNewToken(s: String?) {
        Log.e("NEW_TOKEN", s)
    }


    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.from!!)

        // Check if message contains a notification payload.
        //        if (remoteMessage.getNotification() != null) {
        //            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
        //            handleNotification(remoteMessage.getNotification().getBody());
        //        }

        // Check if message contains a data payload.
        if (remoteMessage.data.size > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.data.toString())

            try {
                val json = JSONObject(remoteMessage.data)
                handleDataMessage(json)
            } catch (e: Exception) {
                Log.e(TAG, "Exception: " + e.message)
            }

        }
    }

    //    private void handleNotification(String message) {
    //        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
    //            // app is in foreground, broadcast the push message
    //            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
    //            pushNotification.putExtra("message", message);
    //            LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
    //
    //            // play notification sound
    //            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
    //            notificationUtils.playNotificationSound();
    //            Log.i("step","1");
    //        }else{
    //            Log.i("step","2");
    //            // If the app is in background, firebase itself handles the notification
    //            Intent resultIntent = new Intent(getApplicationContext(), ProfileActivity.class);
    //            resultIntent.putExtra("message", message);
    //            startActivity(resultIntent);
    //        }
    //    }

    private fun handleDataMessage(json: JSONObject) {
        Log.e(TAG, "push json: $json")

        try {

            val title = json.getString("title")
            val message = json.getString("message")
            val complaintId = json.getString("complaintId")
            val dari = json.getString("dari")
            val apk = json.getString("apk")


            //            boolean isBackground = json.getBoolean("is_background");
            //            String imageUrl = json.getString("image");
            //            JSONObject payload = json.getJSONObject("payload");

            Log.e(TAG, "title: $title")
            Log.e(TAG, "message: $message")
            Log.e(TAG, "complaintId: $complaintId")
            Log.e(TAG, "dari: $dari")
            Log.e(TAG, "apk: $apk")


            //            Log.e(TAG, "isBackground: " + isBackground);
            //            Log.e(TAG, "payload: " + payload.toString());
            //            Log.e(TAG, "imageUrl: " + imageUrl);
            //            Log.e(TAG, "timestamp: " + timestamp);
//            if (Login.isLoggedIn(this)) {
                Log.i("call", "call")
//                if (Integer.parseInt(dari) != Login.getUserId(applicationContext) || apk == "md") {
                    Log.i("call", "call2")

                    if (!NotificationUtils.isAppIsInBackground(applicationContext)) {
                        Log.i("call", "call3")
                        // app is in foreground, broadcast the push message
                        val pushNotification = Intent(Config.PUSH_NOTIFICATION)
                        pushNotification.putExtra("message", message)
                        LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification)

                        //                // play notification sound
                        //                NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
                        //                notificationUtils.playNotificationSound();

                        sendNotificationInfo(message, title, complaintId)


                    } else {
                        val resultIntent = Intent(applicationContext, MainMenuActivity::class.java)
                        resultIntent.putExtra("message", message)


                        sendNotificationInfo(message, title, complaintId)

                    }
//                }
//            }


        } catch (e: JSONException) {
            Log.e(TAG, "Json Exception: " + e.message)
        } catch (e: Exception) {
            Log.e(TAG, "Exception: " + e.message)
        }

    }

    /**
     * Showing notification with text only
     */
    private fun showNotificationMessage(
        context: Context,
        title: String,
        message: String,
        timeStamp: String,
        intent: Intent
    ) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent)
    }

    /**
     * Showing notification with text and image
     */
    private fun showNotificationMessageWithBigImage(
        context: Context,
        title: String,
        message: String,
        timeStamp: String,
        intent: Intent,
        imageUrl: String
    ) {
        notificationUtils = NotificationUtils(context)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        notificationUtils!!.showNotificationMessage(title, message, timeStamp, intent, imageUrl)
    }

    private fun sendNotificationInfo(message: String, title: String, complaintId: String) {
        val requestID = System.currentTimeMillis().toInt()

        val intent = Intent(this, LoginActivity::class.java)
        intent.putExtra("FromNotification", true)
        intent.putExtra("Message", message)
        intent.putExtra("ComplaintId", complaintId)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent =
            PendingIntent.getActivity(this, requestID, intent, PendingIntent.FLAG_ONE_SHOT)

        val m = (Date().time / 1000L % Integer.MAX_VALUE).toInt()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channelId = "BlastChannel1"
            val channelName = "Blast Channel"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val notificationChannel = NotificationChannel(channelId, channelName, importance)
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.BLUE
            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(250, 250, 250, 250)
            notificationManager.createNotificationChannel(notificationChannel)

            val notification = Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(message)
                .setSmallIcon(R.drawable.ic_logo_taisho)
                .setLargeIcon(
                    BitmapFactory.decodeResource(
                        this.resources,
                        R.drawable.ic_logo_taisho
                    )
                ).setContentTitle(title)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setChannelId(channelId)
                .build()
            notificationManager.notify(m, notification)
        } else {
            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_logo_taisho)
                .setLargeIcon(
                    BitmapFactory.decodeResource(
                        this.resources,
                        R.drawable.ic_logo_taisho
                    )
                ).setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setVibrate(longArrayOf(250, 250, 250, 250))
                .setLights(Color.BLUE, 1000, 1000)
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(m, notificationBuilder.build())
        }
    }

    companion object {

        private val TAG = MyFirebaseMessagingService::class.java.simpleName
    }
}