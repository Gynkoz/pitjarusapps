package com.pitjarus.time.reports

import org.apache.http.entity.mime.FormBodyPart
import org.apache.http.entity.mime.content.FileBody
import org.apache.http.entity.mime.content.StringBody
import java.io.File
import java.io.UnsupportedEncodingException

class ReportParameter(var idParameter: String, var idReport: String, var namaParameter: String, var valueParameter: String, var tipeParameter: Int) {

    @Throws(UnsupportedEncodingException::class)
    fun toFormBodyPart(): FormBodyPart {
        return if (tipeParameter == FILE) {
            FormBodyPart(namaParameter, FileBody(File(valueParameter)))
        } else {
            FormBodyPart(namaParameter, StringBody(valueParameter))
        }
    }

    companion object {
        const val TEXT = 0
        const val FILE = 1
    }
}
