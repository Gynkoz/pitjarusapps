package com.pitjarus.time

import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import com.pitjarus.time.database.DaoMaster
import com.pitjarus.time.database.DaoSession
import com.pitjarus.time.reports.DatabaseReport
import com.pitjarus.time.utils.Config
import com.pitjarus.time.utils.StorageUtils
import org.acra.ACRA

import org.acra.ReportField
import org.acra.ReportingInteractionMode
import org.acra.annotation.ReportsCrashes

@ReportsCrashes(
    formUri = Config.CRASH_REPORT_URI,
    customReportContent = [(ReportField.ANDROID_VERSION),
        (ReportField.APP_VERSION_CODE),
        (ReportField.APP_VERSION_NAME),
        (ReportField.AVAILABLE_MEM_SIZE),
        (ReportField.BRAND),
        (ReportField.DEVICE_ID),
        (ReportField.LOGCAT),
        (ReportField.STACK_TRACE),
        (ReportField.PACKAGE_NAME),
        (ReportField.PHONE_MODEL),
        (ReportField.PRODUCT),
        (ReportField.SHARED_PREFERENCES),
        (ReportField.TOTAL_MEM_SIZE),
        (ReportField.USER_COMMENT),
        (ReportField.USER_APP_START_DATE),
        (ReportField.USER_CRASH_DATE),
        (ReportField.USER_EMAIL),
        (ReportField.USER_IP),
        (ReportField.BUILD),
        (ReportField.REPORT_ID),
        (ReportField.DISPLAY),
        (ReportField.SETTINGS_SECURE)],
    mode = ReportingInteractionMode.DIALOG,
    resDialogText = R.string.crash_dialog_text,
    resDialogIcon = android.R.drawable.ic_dialog_info,
    resDialogTitle = R.string.crash_dialog_title,
    resDialogCommentPrompt = R.string.crash_dialog_comment_prompt,
    resDialogOkToast = R.string.crash_dialog_ok_toast,
    resDialogTheme = R.style.AppTheme_Dialog
)
class GlobalApp : Application() {
    var daoSession: DaoSession? = null

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                StorageUtils.createDirectories()
                val database = StorageUtils.getDirectory(StorageUtils.DIRECTORY_DATABASE) + "/" + Config.DATABASE_NAME
                val helper = DaoMaster.DevOpenHelper(this, database)
                val db = helper.writableDb
                daoSession = DaoMaster(db).newSession()
                DatabaseReport.getDatabase(this)
            }
            // Otherwise, call DaoSession on LoginActivity
        } else {
            StorageUtils.createDirectories()
            val database = StorageUtils.getDirectory(StorageUtils.DIRECTORY_DATABASE) + "/" + Config.DATABASE_NAME
            val helper = DaoMaster.DevOpenHelper(this, database)
            val db = helper.writableDb
            daoSession = DaoMaster(db).newSession()
            DatabaseReport.getDatabase(this)
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
//        ACRA.init(this)
    }
}