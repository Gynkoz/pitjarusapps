package com.pitjarus.greendaogenerator;

import org.greenrobot.greendao.generator.DaoGenerator;
import org.greenrobot.greendao.generator.Entity;
import org.greenrobot.greendao.generator.Schema;

public class MainGenerator {
    public static void main(String[] args) throws Exception {
        Schema schema = new Schema(1, "com.pitjarus.time.database");

        //region TransmissionHistory Entity
        Entity transHistory = schema.addEntity("TTransmissionHistory");
        transHistory.addIdProperty();
        transHistory.addStringProperty("Description");
        //endregion

        //region Absensi Entity
        Entity absensi = schema.addEntity("TAbsensi");
        absensi.addIdProperty();
        absensi.addIntProperty("userId");
        absensi.addIntProperty("AbsensiTypeId");
        absensi.addIntProperty("ActivityTypeId");
        absensi.addIntProperty("BobotHarianId");
        absensi.addStringProperty("AbsensiDateTime");
        absensi.addStringProperty("Remarks");
        absensi.addStringProperty("StartDate");
        absensi.addStringProperty("EndDate");
        absensi.addDoubleProperty("Latitude");
        absensi.addDoubleProperty("Longitude");
        absensi.addStringProperty("PhotoPath");
        //endregion


        //region ReasonNoVisit Entity
        Entity reason = schema.addEntity("TReasonNoVisit");
        reason.addIdProperty();
        reason.addIntProperty("ReasonId").notNull();
        reason.addStringProperty("ReasonName").notNull();
        reason.addIntProperty("ReasonGroup").notNull();
        //endregion

//        //region ReasonNoVisit Entity
//        Entity reasonDokter = schema.addEntity("TReasonNoVisitDokter");
//        reasonDokter.addIdProperty();
//        reasonDokter.addIntProperty("ReasonId").notNull();
//        reasonDokter.addStringProperty("ReasonName").notNull();
//        //endregion

        //region Signature Entity
        Entity signature = schema.addEntity("TSignature");
        signature.addIdProperty();
        signature.addStringProperty("SignaturePath").notNull();
        signature.addIntProperty("StoreId").notNull();
        signature.addIntProperty("DokterId").notNull();
        //endregion

        //region ReasonOos Entity
        Entity reasonO = schema.addEntity("TReasonOos");
        reasonO.addIdProperty();
        reasonO.addIntProperty("ReasonId").notNull();
        reasonO.addStringProperty("ReasonName").notNull();
        //endregion

        //region Store Entity
        Entity store = schema.addEntity("TStore");
        store.addIdProperty();
        store.addIntProperty("StoreId").notNull();
        store.addStringProperty("StoreCode").notNull();
        store.addStringProperty("StoreName").notNull();
        store.addStringProperty("Address");
        store.addIntProperty("CityId").notNull();
        store.addStringProperty("CityName").notNull();
        store.addIntProperty("BranchId").notNull();
        store.addStringProperty("BranchName").notNull();
        store.addIntProperty("RegionId").notNull();
        store.addStringProperty("RegionName").notNull();
        store.addIntProperty("CustomerGroupId").notNull();
        store.addStringProperty("CustomerGroupName").notNull();
        store.addIntProperty("ChannelId").notNull();
        store.addStringProperty("ChannelName").notNull();
        store.addIntProperty("StoreClassId").notNull();
        store.addStringProperty("StoreClassName").notNull();
        store.addStringProperty("TerritoryCode").notNull();
        store.addStringProperty("TerritoryName").notNull();
        store.addDoubleProperty("Latitude");
        store.addDoubleProperty("Longitude");
        store.addStringProperty("LastVisited");
        store.addStringProperty("PhotoPath");
        store.addIntProperty("DisplayColumn").notNull();
        store.addIntProperty("DisplayRow").notNull();
        store.addIntProperty("IsVisited").notNull();
        store.addIntProperty("IsMandatory").notNull();
        store.addIntProperty("IsReporting").notNull();
        store.addIntProperty("IsStoreVisit").notNull();
        //endregion

        //region Visit Entity
        Entity rVisit = schema.addEntity("TVisit");
        rVisit.addIdProperty();
        rVisit.addStringProperty("VisitId");
        rVisit.addIntProperty("SurveyorId");
        rVisit.addIntProperty("StoreId");
        rVisit.addIntProperty("IsVisit");
        rVisit.addIntProperty("IsCheckIn");
        rVisit.addStringProperty("ReasonNoVisit");
        rVisit.addLongProperty("StartDateTimeUtc");
        rVisit.addLongProperty("EndDateTimeUtc");
        rVisit.addStringProperty("StartDateTime");
        rVisit.addStringProperty("EndDateTime");
        rVisit.addStringProperty("PhotoPath");
        //endregion

        //region Sku By Customer Group Entity
        Entity sku = schema.addEntity("TSku");
        sku.addIdProperty();
        sku.addIntProperty("SkuId");
        sku.addStringProperty("SkuCode");
        sku.addStringProperty("SkuName");
        sku.addIntProperty("CustomerGroupId");
        sku.addIntProperty("ProductGroupId");
        sku.addStringProperty("ProductGroupName");
        sku.addIntProperty("Ids");
        sku.addIntProperty("BrandId");
        sku.addStringProperty("BrandName");
        sku.addIntProperty("IsIr");
        sku.addIntProperty("IsCompetitor");
        sku.addBooleanProperty("IsDone").notNull();
        //endregion

        //region List Absensi Entity
        Entity ListAbsensi = schema.addEntity("TListAbsensi");
        ListAbsensi.addIdProperty();
        ListAbsensi.addIntProperty("UserId");
        ListAbsensi.addStringProperty("UserCode");
        ListAbsensi.addStringProperty("Username");
        ListAbsensi.addStringProperty("FullName");
        ListAbsensi.addIntProperty("ActivityTypeId");
        ListAbsensi.addStringProperty("ActivityTypeName");
        ListAbsensi.addIntProperty("ParentId");
        ListAbsensi.addStringProperty("StartDate");
        ListAbsensi.addStringProperty("EndDate");
        ListAbsensi.addStringProperty("Remarks");
        ListAbsensi.addStringProperty("PhotoPath");
        ListAbsensi.addIntProperty("ReportAbsensiId");
        ListAbsensi.addIntProperty("IsChecked");
        ListAbsensi.addBooleanProperty("IsDone");
        //endregion

        //region Bobot Harian Type Entity
        Entity bobotHarianType = schema.addEntity("TBobotHarianType");
        bobotHarianType.addIdProperty();
        bobotHarianType.addIntProperty("BobotId");
        bobotHarianType.addStringProperty("BobotName");
        //endregion

        //region Approval Store
        Entity appovalStore = schema.addEntity("TApprovalStore");
        appovalStore.addIdProperty();
        appovalStore.addIntProperty("UserId");
        appovalStore.addStringProperty("UserName");
        appovalStore.addStringProperty("FullName");
        appovalStore.addStringProperty("DokterName");
        appovalStore.addIntProperty("IsApproved");
        appovalStore.addIntProperty("StoreId");
        appovalStore.addStringProperty("StoreName");
        appovalStore.addStringProperty("Address");
        appovalStore.addStringProperty("Date");
        appovalStore.addStringProperty("SpesialisName");
        appovalStore.addStringProperty("ClassName");
        appovalStore.addIntProperty("JourneyPlanId");
        appovalStore.addIntProperty("IsChecked");
        //endregion


        //region Activity Type Entity
        Entity activityType = schema.addEntity("TActivityType");
        activityType.addIdProperty();
        activityType.addIntProperty("ActivityTypeId");
        activityType.addStringProperty("ActivityTypeName");
        //endregion

        //region Brand Entity
        Entity brands = schema.addEntity("TBrands");
        brands.addIdProperty();
        brands.addIntProperty("CustomerGroupId");
        brands.addIntProperty("BrandId");
        brands.addStringProperty("BrandName");
        brands.addBooleanProperty("IsDone").notNull();
        //endregion

        //region Promo Type Entity
        Entity promoType = schema.addEntity("TPromoType");
        promoType.addIdProperty();
        promoType.addIntProperty("PromoTypeId");
        promoType.addStringProperty("PromoTypeName");
        //endregion

        //region Competitor Price Entity
        Entity competitorPrice = schema.addEntity("TCompetitorPrice");
        competitorPrice.addIdProperty();
        competitorPrice.addIntProperty("CompetitorPriceId");
        competitorPrice.addStringProperty("CompetitorPriceName");
        //endregion

        //region Competitor Product Entity
        Entity competitorProduct = schema.addEntity("TCompetitorProduct");
        competitorProduct.addIdProperty();
        competitorProduct.addIntProperty("CompetitorProductId");
        competitorProduct.addStringProperty("CompetitorProductName");
        //endregion

        //region Report Addiotional Promo Entity
        Entity reportAdditionalPromo = schema.addEntity("TReportAdditionalPromo");
        reportAdditionalPromo.addIdProperty();
        reportAdditionalPromo.addIntProperty("QuestionId1");
        reportAdditionalPromo.addIntProperty("QuestionId2");
        reportAdditionalPromo.addStringProperty("PhotoPath");
        //endregion


        //region Dokter Entity
        Entity dokter = schema.addEntity("TDokter");
        dokter.addIdProperty();
        dokter.addIntProperty("DokterId");
        dokter.addIntProperty("StoreId");
        dokter.addStringProperty("DokterCode");
        dokter.addStringProperty("DokterName");
        dokter.addStringProperty("ClassName");
        dokter.addStringProperty("SpecialityName");
        dokter.addStringProperty("SubSpecialityName");
        dokter.addStringProperty("Signature1");
        dokter.addStringProperty("Signature1Date");
        dokter.addStringProperty("Signature2");
        dokter.addStringProperty("Signature2Date");
        dokter.addIntProperty("IsDokterVisit");
        dokter.addBooleanProperty("IsDone");
        //endregion

        //region Visit Entity
        Entity rVisitDokter = schema.addEntity("TVisitDokter");
        rVisitDokter.addIdProperty();
        rVisitDokter.addStringProperty("VisitId");
        rVisitDokter.addStringProperty("VisitDokterId");
        rVisitDokter.addIntProperty("SurveyorId");
        rVisitDokter.addIntProperty("StoreId");
        rVisitDokter.addIntProperty("DokterId");
        rVisitDokter.addIntProperty("IsVisit");
        rVisitDokter.addIntProperty("IsCheckIn");
        rVisitDokter.addStringProperty("ReasonNoVisit");
        rVisitDokter.addLongProperty("StartDateTimeUtc");
        rVisitDokter.addLongProperty("EndDateTimeUtc");
        rVisitDokter.addStringProperty("StartDateTime");
        rVisitDokter.addStringProperty("EndDateTime");
        rVisitDokter.addStringProperty("Info");
        rVisitDokter.addStringProperty("SignaturePath");
        rVisitDokter.addBooleanProperty("IsDone");

        //endregion

        //region ReportSku Entity
        Entity rProduct = schema.addEntity("TReportSku");
        rProduct.addIdProperty();
        rProduct.addStringProperty("ReportId").notNull();
        rProduct.addStringProperty("VisitId").notNull();
        rProduct.addStringProperty("HeaderId").notNull();
        rProduct.addIntProperty("StoreId").notNull();
        rProduct.addIntProperty("UserId").notNull();
        rProduct.addIntProperty("SkuId").notNull();
        rProduct.addStringProperty("SkuCode").notNull();
        rProduct.addStringProperty("SkuName").notNull();
        rProduct.addIntProperty("IsIr").notNull();
        rProduct.addIntProperty("CustomerGroupId").notNull();
        rProduct.addIntProperty("BrandId").notNull();
        rProduct.addStringProperty("BrandName").notNull();
        rProduct.addStringProperty("ImagePath");
        rProduct.addIntProperty("Quantity").notNull();
        rProduct.addIntProperty("Depth").notNull();
        rProduct.addIntProperty("IsCompetitor");
        rProduct.addBooleanProperty("IsDone").notNull();
        rProduct.addBooleanProperty("IsMedrep").notNull();
        //endregion

        //Report Display Header Entity
        Entity rDisplayHeader = schema.addEntity("TReportDisplayHeader");
        rDisplayHeader.addIdProperty();
        rDisplayHeader.addStringProperty("HeaderId").notNull();
        rDisplayHeader.addStringProperty("PemasanganId").notNull();
        rDisplayHeader.addStringProperty("VisitId").notNull();
        rDisplayHeader.addIntProperty("StoreId").notNull();
        rDisplayHeader.addIntProperty("UserId").notNull();
        rDisplayHeader.addIntProperty("BrandId").notNull();
        rDisplayHeader.addIntProperty("PhotoRowSize").notNull();
        rDisplayHeader.addIntProperty("PhotoColumnSize").notNull();
        rDisplayHeader.addBooleanProperty("IsSaved").notNull();
        rDisplayHeader.addBooleanProperty("IsDone").notNull();

        //Report Display Detail Entity
        Entity rDisplayDetail = schema.addEntity("TReportDisplayDetail");
        rDisplayDetail.addIdProperty();
        rDisplayDetail.addStringProperty("ReportDetailId").notNull();
        rDisplayDetail.addStringProperty("ReportHeaderId").notNull();
        rDisplayDetail.addStringProperty("VisitId").notNull();
        rDisplayDetail.addStringProperty("PhotoPath").notNull();
        rDisplayDetail.addIntProperty("BrandId").notNull();
        rDisplayDetail.addIntProperty("ColumnNumber").notNull();
        rDisplayDetail.addIntProperty("RowNumber").notNull();
        rDisplayDetail.addIntProperty("LeftPosition").notNull();
        rDisplayDetail.addIntProperty("TopPosition").notNull();
        rDisplayDetail.addIntProperty("RightPosition").notNull();
        rDisplayDetail.addIntProperty("BottomPosition").notNull();
        rDisplayDetail.addBooleanProperty("IsDone").notNull();

        //Report Kunjungan Dokter Entity
        Entity rKunjungan = schema.addEntity("TKunjunganDokter");
        rKunjungan.addIdProperty();
        rKunjungan.addStringProperty("ReportKunjunganId").notNull();
        rKunjungan.addStringProperty("Notes").notNull();
        rKunjungan.addStringProperty("VisitId").notNull();
        rKunjungan.addStringProperty("SignaturePath").notNull();
        rKunjungan.addBooleanProperty("IsDone").notNull();

        //Survey Promo Entity
        Entity sPromo = schema.addEntity("TSurveyPromo");
        sPromo.addIdProperty();
        sPromo.addIntProperty("UserId");
        sPromo.addIntProperty("CustomerGroupId");
        sPromo.addIntProperty("PromoId");
        sPromo.addStringProperty("PromoQuestion");
        sPromo.addIntProperty("PromoAnswerType");
        sPromo.addStringProperty("VisitId");

        //Survey Promo Answer Entity
        Entity sPromoAnwer = schema.addEntity("TSurveyAnswerPromo");
        sPromoAnwer.addIdProperty();
        sPromoAnwer.addStringProperty("ReportId");
        sPromoAnwer.addIntProperty("UserId");
        sPromoAnwer.addIntProperty("CustomerGroupId");
        sPromoAnwer.addIntProperty("PromoId");
        sPromoAnwer.addStringProperty("PromoQuestion");
        sPromoAnwer.addIntProperty("PromoAnswerType");
        sPromoAnwer.addStringProperty("VisitId");
        sPromoAnwer.addIntProperty("IsChecked");
        sPromoAnwer.addIntProperty("ReasonId");
        sPromoAnwer.addStringProperty("ReasonName");

        //Survey Competitor Product Answer Entity
        Entity sCompetitorProductAnswer = schema.addEntity("TSurveyCompetitorProductAnswer");
        sCompetitorProductAnswer.addIdProperty();
        sCompetitorProductAnswer.addStringProperty("ReportId");
        sCompetitorProductAnswer.addIntProperty("UserId");
        sCompetitorProductAnswer.addIntProperty("CustomerGroupId");
        sCompetitorProductAnswer.addIntProperty("CompetitorProductId");
        sCompetitorProductAnswer.addStringProperty("CompetitorProductQuestion");
        sCompetitorProductAnswer.addIntProperty("CompetitorProductAnswerType");
        sCompetitorProductAnswer.addStringProperty("VisitId");
        sCompetitorProductAnswer.addIntProperty("ReasonId");
        sCompetitorProductAnswer.addStringProperty("ReasonName");
        sCompetitorProductAnswer.addIntProperty("IsChecked");

        //Survey Competitor Product Answer Entity
        Entity sCompetitorProduct = schema.addEntity("TSurveyCompetitorProduct");
        sCompetitorProduct.addIdProperty();
        sCompetitorProduct.addIntProperty("UserId");
        sCompetitorProduct.addIntProperty("CustomerGroupId");
        sCompetitorProduct.addIntProperty("CompetitorProductId");
        sCompetitorProduct.addStringProperty("CompetitorProductQuestion");
        sCompetitorProduct.addIntProperty("CompetitorProductAnswerType");
        sCompetitorProduct.addStringProperty("VisitId");


        //Survey Competitor Price Answer Entity
        Entity sCompetitorPriceAnswer = schema.addEntity("TSurveyCompetitorPriceAnswer");
        sCompetitorPriceAnswer.addIdProperty();
        sCompetitorPriceAnswer.addStringProperty("ReportId");
        sCompetitorPriceAnswer.addIntProperty("UserId");
        sCompetitorPriceAnswer.addIntProperty("CustomerGroupId");
        sCompetitorPriceAnswer.addIntProperty("CompetitorPriceId");
        sCompetitorPriceAnswer.addStringProperty("CompetitorPriceQuestion");
        sCompetitorPriceAnswer.addIntProperty("CompetitorPriceAnswerType");
        sCompetitorPriceAnswer.addStringProperty("VisitId");
        sCompetitorPriceAnswer.addIntProperty("ReasonId");
        sCompetitorPriceAnswer.addStringProperty("ReasonName");
        sCompetitorPriceAnswer.addIntProperty("IsChecked");

        //Survey Competitor Price Entity
        Entity sCompetitorPrice = schema.addEntity("TSurveyCompetitorPrice");
        sCompetitorPrice.addIdProperty();
        sCompetitorPrice.addIntProperty("UserId");
        sCompetitorPrice.addIntProperty("CustomerGroupId");
        sCompetitorPrice.addIntProperty("CompetitorPriceId");
        sCompetitorPrice.addStringProperty("CompetitorPriceQuestion");
        sCompetitorPrice.addIntProperty("CompetitorPriceAnswerType");
        sCompetitorPrice.addStringProperty("VisitId");

        //region Choice Entity
        Entity choice = schema.addEntity("TChoice");
        choice.addIdProperty();
        choice.addLongProperty("ChoiceId").notNull();
        choice.addLongProperty("ChoiceReferenceId");
        choice.addLongProperty("ChoiceReferenceGroupId");
        choice.addIntProperty("QuestionerId").notNull();
        choice.addLongProperty("QuestionId").notNull();
        choice.addStringProperty("ChoiceName").notNull();
        //endregion

        //region Question Entity
        Entity question = schema.addEntity("TQuestion");
        question.addIdProperty();
        question.addLongProperty("QuestionId").notNull();
        question.addIntProperty("QuestionerId").notNull();
        question.addStringProperty("QuestionName").notNull();
        question.addStringProperty("QuestionType").notNull();
        question.addIntProperty("QuestionNumber").notNull();
        question.addIntProperty("IsRequired").notNull();
        question.addIntProperty("IsCascaded").notNull();
        question.addIntProperty("HasChild").notNull();
        question.addIntProperty("ChildCount").notNull();
        question.addIntProperty("ParentQuestionIsRequired").notNull();
        question.addIntProperty("ParentQuestionIsRequiredNumber");
        question.addStringProperty("ParentQuestionIsRequiredAnswer");
        //endregion

        //region Questioner Entity
        Entity questioner = schema.addEntity("TQuestioner");
        questioner.addIdProperty();
        questioner.addIntProperty("QuestionerId").notNull();
        questioner.addStringProperty("QuestionerName").notNull();
        questioner.addStringProperty("SubTitle").notNull();
        questioner.addStringProperty("StartDate").notNull();
        questioner.addStringProperty("EndDate").notNull();
        questioner.addIntProperty("SortingNumber").notNull();

        //region Respondent Entity
        Entity respondent = schema.addEntity("TReportRespondent");
        respondent.addIdProperty();
        respondent.addIntProperty("QuestionerId").notNull();
        respondent.addStringProperty("QuestionerName").notNull();
        respondent.addStringProperty("SubTitle").notNull();
        respondent.addStringProperty("QuestionerDate").notNull();
        respondent.addIntProperty("SurveyorId").notNull();
        respondent.addStringProperty("StoreId").notNull();
        respondent.addStringProperty("VisitId").notNull();
        respondent.addIntProperty("SortingNumber").notNull();
        respondent.addStringProperty("RespondentName").notNull();
        respondent.addStringProperty("RespondentPhone").notNull();
        respondent.addBooleanProperty("IsDone").notNull();
        //endregion

        //region Answer Entity
        Entity answer = schema.addEntity("TReportAnswer");
        answer.addIdProperty();
        answer.addIntProperty("QuestionerId").notNull();
        answer.addLongProperty("RespondentId").notNull();
        answer.addLongProperty("QuestionId").notNull();
        answer.addStringProperty("QuestionName").notNull();
        answer.addStringProperty("QuestionType").notNull();
        answer.addIntProperty("QuestionNumber").notNull();
        answer.addIntProperty("IsRequired").notNull();
        answer.addIntProperty("IsCascaded").notNull();
        answer.addIntProperty("IsEnabled").notNull();
        answer.addIntProperty("HasChild").notNull();
        answer.addIntProperty("ChildCount").notNull();
        answer.addIntProperty("ParentQuestionIsRequired").notNull();
        answer.addIntProperty("ParentQuestionIsRequiredNumber");
        answer.addStringProperty("ParentQuestionIsRequiredAnswer");
        answer.addLongProperty("ChoiceId");
        answer.addLongProperty("ChoiceReferenceId");
        answer.addLongProperty("ChoiceReferenceGroupId");
        answer.addStringProperty("AnswerName").notNull();
        answer.addIntProperty("SurveyorId").notNull();
        answer.addStringProperty("StoreId").notNull();
        answer.addStringProperty("VisitId").notNull();
        answer.addStringProperty("ImagePath");
        //endregion

        //region ReportScan Entity
        Entity tScan = schema.addEntity("TReportScan");
        tScan.addIdProperty();
        tScan.addStringProperty("VisitId").notNull();
        tScan.addStringProperty("ReportDetailId").notNull();
        tScan.addStringProperty("PhotoPath").notNull();
        tScan.addIntProperty("CategoryId").notNull();
        tScan.addIntProperty("ProductId").notNull();

        new DaoGenerator().generateAll(schema, "../app/src/main/java");
    }
}
